package org.shikimori.c7j.rec.data;


import java.io.Serializable;


@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Screenshot implements Serializable {

    private String original;
    private String preview;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

}
