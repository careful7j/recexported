package org.shikimori.c7j.rec.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Fragment;

import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.utils.FragmentInfo;



public class EmptyFragment extends Fragment {

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.EMPTY);
    public FragmentInfo getFragmentInfo() { return fragmentInfo; }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View holder = inflater.inflate(R.layout.fragment_empty, container, false);
        return holder;
    }
}