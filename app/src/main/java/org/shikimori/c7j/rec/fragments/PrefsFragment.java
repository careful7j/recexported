package org.shikimori.c7j.rec.fragments;


import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.AppSettings;
import org.shikimori.c7j.rec.MyApp;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.utils.FragmentInfo;

import static org.shikimori.c7j.rec.MyApp.fontNames;

public class PrefsFragment extends PreferenceFragment {

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.PREFS);
    public FragmentInfo getFragmentInfo() { return fragmentInfo; }


    private void setUpListPreferences() throws Exception {
        final ListPreference yearFilter = (ListPreference) findPreference(AppSettings.KEY_YEAR_FILTER);
        yearFilter.setTitle(getResources().getString(R.string.frag_settings_select_year) +
                getResources().getStringArray(R.array.year_entities)[Integer.parseInt(yearFilter.getValue())]);

        yearFilter.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                yearFilter.setTitle(getResources().getString(R.string.frag_settings_select_year) +
                        getResources().getStringArray(R.array.year_entities)[Integer.parseInt(newValue.toString())]);
                return true;
            }
        });

        final ListPreference fontMode = (ListPreference) findPreference(AppSettings.KEY_FONT_MODE);
        fontMode.setTitle(getResources().getString(R.string.frag_settings_select_font) +
                getResources().getStringArray(R.array.font_entities)[Integer.parseInt(fontMode.getValue())]);

        fontMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                fontMode.setTitle(getResources().getString(R.string.frag_settings_select_font) +
                        getResources().getStringArray(R.array.font_entities)[Integer.parseInt(newValue.toString())]);
                applyFont(newValue.toString());
                return true;
            }
        });

        final ListPreference orderByMode = (ListPreference) findPreference(AppSettings.KEY_ORDER_BY_MODE);
        orderByMode.setTitle(getResources().getString(R.string.frag_settings_select_orderby) +
                getResources().getStringArray(R.array.orderby_entities)[Integer.parseInt(orderByMode.getValue())]);

        orderByMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                orderByMode.setTitle(getResources().getString(R.string.frag_settings_select_orderby) +
                        getResources().getStringArray(R.array.orderby_entities)[Integer.parseInt(newValue.toString())]);
                return true;
            }
        });
    }



    private void applyFont(String fontName) {
        View view = getView();
        if (view != null) {
            MyApp.setAppFont(getView().getContext(), fontNames[Integer.parseInt(fontName)]);
            ( (ActivitySearch) getActivity() ).onBackPressed(); //do not fix this
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        try {
            setUpListPreferences();
        } catch (Exception e) { GAnalytics.reportUnexpectedError("setUpListPreferences()", e.toString()); }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) view.setBackgroundColor(getResources().getColor(android.R.color.white));
        return view;
    }


}