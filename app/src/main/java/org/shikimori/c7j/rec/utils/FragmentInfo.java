package org.shikimori.c7j.rec.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class FragmentInfo implements Serializable {

    public static final int UNDEFINED = 0;
    public static final int ANIME_INFO = 1;
    public static final int SEARCH_RESULTS = 2;
    public static final int TABHOLDER = 3;
    public static final int STARTING= 4;
    public static final int AUTH = 5;
    public static final int SETTINGS = 6;
    public static final int EMPTY = 7;
    public static final int PREFS = 8;


    private int fragmentType;
    private List<ResponseInfo> responses = new ArrayList<>();


    public FragmentInfo(int fragmentType) {
        this.fragmentType = fragmentType;
    }


    public int getFragmentType() {
        return fragmentType;
    }


    public void setFragmentType(int fragmentType) {
        this.fragmentType = fragmentType;
    }


    public List<ResponseInfo> getResponses() {
        return responses;
    }


    public void addResponse(ResponseInfo responseInfo) {
        responses.add(responseInfo);
    }
}
