package org.shikimori.c7j.rec.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.shikimori.c7j.rec.R;


public class UIUtils {

    private static Toast toastMessage = null;

    /** Custom toastMessage that shows something */
    public static void showCustomToast(Activity activity, @LayoutRes int layoutRes, int lengthOfToast) {

        if (toastMessage != null) {
            toastMessage.cancel();
        }
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate( layoutRes,
                (ViewGroup) activity.findViewById( R.id.toast_layout ) );
        toastMessage = new Toast( activity );
        toastMessage.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toastMessage.setDuration(lengthOfToast);
        toastMessage.setView(layout);
        toastMessage.show();
    }


    /** Forwards user to applications play-market page */
    public static void openAppGooglePlayMarketPage(Activity activity) {
        final String appPackageName = activity.getPackageName();
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

}
