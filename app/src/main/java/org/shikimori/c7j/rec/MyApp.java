package org.shikimori.c7j.rec;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

//import com.crashlytics.android.Crashlytics;

import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.utils.BackStackUtils;
import org.shikimori.c7j.rec.utils.L;

//import io.fabric.sdk.android.Fabric;



public class MyApp extends Application {

    public static final String[] fontNames = { "Pt-Serif.ttf", "Roboto-Regular.ttf", "OpenSans-Regular.ttf", "DejaVu-Sans.ttf" };

    /** Custom backStack implementation to provide a better user experience to our users */
    private BackStackUtils backStackUtils = new BackStackUtils();
    private AppSettings appSettings = null;

    /** Custom appDefaultFont for all FontTextViews */
    private static Typeface appDefaultFont;


    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getApplicationContext();
        if (!org.shikimori.c7j.rec.BuildConfig.BUILD_TYPE.equals("debug")) {
//            Fabric.with(context, new Crashlytics());
            GAnalytics.initialize(context);
        }
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false); //sets default values on first launch
        appSettings = new AppSettings(context);
        setUpAppFont(context);
    }


    private void setUpAppFont(Context context) {
        if (!appSettings.readIsFontUpdatedTo100()) {
            appSettings.writeFontMode("1");
            appSettings.writeIsFontUpdatedTo100(true);
        }
        setAppFont(context, fontNames[Integer.parseInt(appSettings.readFontMode())]);
    }


    public static Typeface getAppDefaultFont() { return appDefaultFont; }


    public static void setAppFont(Context context, String typefaceName) {
        appDefaultFont = Typeface.createFromAsset( context.getAssets(), typefaceName );
    }


    public AppSettings getAppSettings() {
        return appSettings;
    }


    public BackStackUtils getBackStackUtils() {
        return backStackUtils;
    }

}
