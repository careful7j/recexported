package org.shikimori.c7j.rec.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.shikimori.c7j.rec.CoreActivity;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.data.roles.Character;
import org.shikimori.c7j.rec.data.roles.Image;
import org.shikimori.c7j.rec.data.roles.Role;
import org.shikimori.c7j.rec.network.ShikimoriService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.shikimori.c7j.rec.CoreActivity.LOCALE_RUSSIAN;


public class AdapterCharacters extends RecyclerView.Adapter<AdapterCharacters.ViewHolder> {

    private List<Role> dataSet;


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvCharacterName;
        ImageView imgCharacterPoster;


        ViewHolder(View v) {
            super(v);
            tvCharacterName = (TextView) v.findViewById(R.id.tv_character_name);
            imgCharacterPoster = (ImageView) v.findViewById(R.id.img_character_poster);
        }
    }


    public AdapterCharacters( ArrayList<Role> dataSet) {
        this.dataSet = dataSet;
    }


    public void updateDataSet( List<Role> dataSet ) {
        this.dataSet = dataSet;
        this.notifyDataSetChanged();
    }


    @Override
    public AdapterCharacters.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_character, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterCharacters.ViewHolder holder, int position) {
        String txtCharacterName = "???";
        Character character = dataSet.get(position).getCharacter();

        if ( character != null ) {

            if ( CoreActivity.locale.equals(CoreActivity.LOCALE_RUSSIAN)) {
                txtCharacterName = character.getRussian();
            } else {
                txtCharacterName = character.getName();
            }
            holder.tvCharacterName.setText(txtCharacterName);

            Context context = holder.tvCharacterName.getRootView().getContext();
            Image image = character.getImage();
            if ( image != null ) {
                Glide.with(context)
                        .load(ShikimoriService.WEBSERVICE_BASE_URL + image.getOriginal())
                        .centerCrop()
                        .into(holder.imgCharacterPoster);
            }
        }
    }


    @Override
    public int getItemCount() {
        if (dataSet == null) {
            return 0;
        } else return dataSet.size();
    }
}
