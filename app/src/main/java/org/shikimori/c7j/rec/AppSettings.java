package org.shikimori.c7j.rec;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/** All the application settings should be stored are accessed here */
@SuppressWarnings("WeakerAccess")
public class AppSettings {

    /** WARNING: If you change this KEYS - also update changes to res/xml/preferences.xml */

    /** Show TV series in search results */
    private static final String KEY_TYPE_TV = "KEY_TYPE_TV";
    /** Show OVA and Specials in search results */
    private static final String KEY_TYPE_OVA = "KEY_TYPE_OVA";
    /** Show ONA in search results */
    private static final String KEY_TYPE_ONA = "KEY_TYPE_ONA";
    /** Show Movies in search results */
    private static final String KEY_TYPE_MOVIE = "KEY_TYPE_MOVIE";

    /** Show only animes released not earlier than this year value  */
    public static final String KEY_YEAR_FILTER = "KEY_YEAR_FILTER";

    /** Stores the name of the font which application uses */
    public static final String KEY_FONT_MODE = "KEY_FONT_MODE";

    /** Stores the name of the search results sorting order which application uses */
    public static final String KEY_ORDER_BY_MODE = "KEY_ORDER_BY_MODE";

    /** Show only animes that are neither ongoing nor announced  */
    private static final String KEY_IS_NOT_ONGOING = "KEY_IS_NOT_ONGOING";

    /* If true app restores last session when launched, if false starts from the starting fragment */
    private static final String KEY_RESTORE_LAST_SESSION = "KEY_RESTORE_LAST_SESSION";
    /* Ignore system language and always start application with Russian language */
    public static final String KEY_ALWAYS_USE_RUSSIAN = "KEY_ALWAYS_USE_RUSSIAN";
    /* Restore session with 3 backStack entities instead of 30 */
    private static final String KEY_IS_RESTORE_BACKSTACK_SHORT = "KEY_IS_RESTORE_BACKSTACK_SHORT";

    /** When enabled, number of screenshots shown at animeInfo fragment is limited by defined number */
    private static final String KEY_IS_SCREENSHOTS_LIMIT_SET = "KEY_IS_SCREENSHOTS_LIMIT_SET";
    /** When enabled, if you are connected via mobile network screenshots are not loading (to reduce traffic consumption) */
    private static final String KEY_IS_DONT_LOAD_SCREENSHOTS_OVER_3G = "KEY_IS_DONT_LOAD_SCREENSHOTS_OVER_3G";

    /** Store user's access token (authorization) */
    private static final String KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN";
    /** Store user's shikimori id */
    private static final String KEY_USER_ID = "KEY_USER_ID";
    /** Store user's shikimori nickname */
    private static final String KEY_USER_NICKNAME = "KEY_USER_NICKNAME";

    /** How many times search request were sent to shikimori service */
    private static final String KEY_SEARCH_API_CALLS_COUNTER = "KEY_SEARCH_API_CALLS_COUNTER";

    /** Update font set to users updating to v.1.00  */
    private static final String KEY_IS_FONT_UPDATED_TO_100 = "KEY_IS_FONT_UPDATED_TO_100";


    private Context context;
    private SharedPreferences prefs;

    public AppSettings(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }


    private SharedPreferences.Editor getEditor() {
        return PreferenceManager.getDefaultSharedPreferences(context).edit();
    }

    //========================================================
    //  R E A D
    //========================================================

    public boolean readTypeTv() {
        return prefs.getBoolean(KEY_TYPE_TV, true);
    }

    public boolean readTypeOva() {
        return prefs.getBoolean(KEY_TYPE_OVA, false);
    }

    public boolean readTypeOna() {
        return prefs.getBoolean(KEY_TYPE_ONA, true);
    }

    public boolean readTypeMovie() {
        return prefs.getBoolean(KEY_TYPE_MOVIE, true);
    }

    public String readYearFilter() {
        return prefs.getString(KEY_YEAR_FILTER, "0");
    }

    public String readFontMode() { return prefs.getString(KEY_FONT_MODE, "1"); }

    public String readOrderBy() { return prefs.getString(KEY_ORDER_BY_MODE, "0"); }

    public boolean readIsNotOngoing() {
        return prefs.getBoolean(KEY_IS_NOT_ONGOING, false);
    }

    public boolean readRestoreLastSession() { return prefs.getBoolean(KEY_RESTORE_LAST_SESSION, true); }

    public boolean readAlwaysUseRussian() { return prefs.getBoolean(KEY_ALWAYS_USE_RUSSIAN, false); }

    public boolean readIsRestoreBackStackShort() { return prefs.getBoolean(KEY_IS_RESTORE_BACKSTACK_SHORT, false); }

    public boolean readIsScreenshotsLimitSet() { return prefs.getBoolean(KEY_IS_SCREENSHOTS_LIMIT_SET, true); }

    public boolean readIsDontLoadScreenshotsOver3G() { return prefs.getBoolean(KEY_IS_DONT_LOAD_SCREENSHOTS_OVER_3G, false); }

    public String readAccessToken() {
        return prefs.getString(KEY_ACCESS_TOKEN, "");
    }

    public String readUserId() {
        return prefs.getString(KEY_USER_ID, "");
    }

    public String readUserNickname() {
        return prefs.getString(KEY_USER_NICKNAME, "");
    }

    public long readSearchApiCallsCounter() { return prefs.getLong(KEY_SEARCH_API_CALLS_COUNTER, 0); }

    public boolean readIsFontUpdatedTo100() { return prefs.getBoolean(KEY_IS_FONT_UPDATED_TO_100, false); }

    //========================================================
    //  W R I T E
    //========================================================

    public void writeFontMode(String fontMode) {
        getEditor()
                .putString(KEY_FONT_MODE, fontMode)
                .commit();
    }

    public void writeAccessToken(String accessToken) {
        getEditor()
                .putString(KEY_ACCESS_TOKEN, accessToken)
                .commit();
    }

    public void writeUserId(String userId) {
        getEditor()
                .putString(KEY_USER_ID, userId)
                .commit();
    }

    public void writeUserNickname(String userNickname) {
        getEditor()
                .putString(KEY_USER_NICKNAME, userNickname)
                .commit();
    }

    public void writeSearchApiCallsCounter(long searchApiCallsCounter) {
        getEditor()
                .putLong(KEY_SEARCH_API_CALLS_COUNTER, searchApiCallsCounter)
                .commit();
    }

    public void writeIsFontUpdatedTo100(boolean flag) {
        getEditor()
                .putBoolean(KEY_IS_FONT_UPDATED_TO_100, flag)
                .commit();
    }

}
