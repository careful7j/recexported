
package org.shikimori.c7j.rec.data.animerates;

import java.io.Serializable;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class AnimeRate implements Serializable {

    private Integer id;
    private Integer score;
    private String status;
    private Object text;
    private Integer episodes;
    private Integer chapters;
    private Integer volumes;
    private Object textHtml;
    private Integer rewatches;
    private User user;
    private Anime anime;
    private Object manga;


    public AnimeRate() { /* Empty constructor */ }

    /** Constructor to create a fake AnimeRate for local cache (after Save button is pressed by user) */
    public AnimeRate(Integer animeId, String status) {
        this.id = -1;
        this.status = status;
        Anime anime = new Anime();
        anime.setId(animeId);
        this.anime = anime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getText() {
        return text;
    }

    public void setText(Object text) {
        this.text = text;
    }

    public Integer getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Integer episodes) {
        this.episodes = episodes;
    }

    public Integer getChapters() {
        return chapters;
    }

    public void setChapters(Integer chapters) {
        this.chapters = chapters;
    }

    public Integer getVolumes() {
        return volumes;
    }

    public void setVolumes(Integer volumes) {
        this.volumes = volumes;
    }

    public Object getTextHtml() {
        return textHtml;
    }

    public void setTextHtml(Object textHtml) {
        this.textHtml = textHtml;
    }

    public Integer getRewatches() {
        return rewatches;
    }

    public void setRewatches(Integer rewatches) {
        this.rewatches = rewatches;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Anime getAnime() {
        return anime;
    }

    public void setAnime(Anime anime) {
        this.anime = anime;
    }

    public Object getManga() {
        return manga;
    }

    public void setManga(Object manga) {
        this.manga = manga;
    }


}
