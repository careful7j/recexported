package org.shikimori.c7j.rec.network;

import org.shikimori.c7j.rec.data.AccessToken;
import org.shikimori.c7j.rec.data.Related;
import org.shikimori.c7j.rec.data.Screenshot;
import org.shikimori.c7j.rec.data.animeinfo.AnimeInfo;
import org.shikimori.c7j.rec.data.animerates.AnimeRate;
import org.shikimori.c7j.rec.data.roles.Role;
import org.shikimori.c7j.rec.data.search.SearchResult;
import org.shikimori.c7j.rec.data.userrate.UserRate;
import org.shikimori.c7j.rec.data.userrate.UserRateSend;
import org.shikimori.c7j.rec.data.whoami.WhoAmI;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Path;


/** All API calls to shikimori webservice shall be placed here */
public interface ShikimoriApi {

    int REQUEST_SEARCH = 0;
    int REQUEST_ANIMEINFO = 1;
    int REQUEST_ROLES = 2;
    int REQUEST_SIMILAR = 3;
    int REQUEST_ACCESS_TOKEN = 4;       // Don't add to reconnection master
    int REQUEST_WHOAMI = 5;
    int REQUEST_GET_ALL_ANIME_RATES = 6;
    int REQUEST_GET_ALL_ANIME_RATES_SAVE = 7;
    int REQUEST_CREATE_USER_RATE = 8;
    int REQUEST_SEARCH_BY_STATUS = 9;
    int REQUEST_SEARCH_BY_GENRE = 10;
    int REQUEST_SEARCH_BY_TYPE = 11;
    int REQUEST_SEARCH_RANDOM = 12;
    int REQUEST_RELATED = 13;
    int REQUEST_SCREENSHOTS = 14;

    int TOTAL_NUMBER_OF_REQUESTS = 15;  // Should include all the requests listed above

    int SEARCH_QUERY_LIMIT = 50;        // Max number of items returned by search query, server side limit 50

    String SEARCH_ORDER_BY_POPULARITY = "popularity";
    String SEARCH_ORDER_BY_USER_RATING = "ranked";
    String SEARCH_STATUS_ONGOING = "ongoing";
    String SEARCH_STATUS_LATEST = "latest";
    String SEARCH_STATUS_ANNOUNCED = "anons";
    String SEARCH_TYPE_MOVIE = "movie";

    String KIND_TV = "tv";
    String KIND_OVA = "ova";
    String KIND_ONA = "ona";
    String KIND_MOVIE = "movie";
    String KIND_SPECIAL = "special";

    String API_REQUEST_SEARCH = "/api/animes/search?";
    String API_REQUEST_ANIMEINFO = "/api/animes/{id}";
    String API_REQUEST_ROLES = "/api/animes/{id}/roles";
    String API_REQUEST_SIMILAR = "/api/animes/{id}/similar";
    String API_REQUEST_ACCESS_TOKEN = "/api/access_token?";
    String API_REQUEST_WHOAMI = "/api/users/whoami";
    String API_REQUEST_GET_ALL_ANIME_RATES = "/api/users/{user_id}/anime_rates?";
    String API_REQUEST_CREATE_USER_RATE = "/api/v2/user_rates/{id}";
    String API_REQUEST_SEARCH_BY_STATUS = "/api/animes?";
    String API_REQUEST_SEARCH_BY_GENRE = "/api/animes?";
    String API_REQUEST_SEARCH_BY_TYPE = "/api/animes?";
    String API_REQUEST_SEARCH_RANDOM = "/api/animes?";
    String API_REQUEST_RELATED = "/api/animes/{id}/related";
    String API_REQUEST_SCREENSHOTS = "/api/animes/{id}/screenshots";


    @GET("/api/animes?")
    Observable<List<SearchResult>> getSearchResult(
            @Query("q") String query,
            @Query("limit") int limit,
            @Query("season") String season,
            @Query("type") String type,
            @Query("order") String order,
            @Query("censored") boolean censored
    );


    @GET("/api/animes/{id}")
    Observable<AnimeInfo> getAnimeInfo(
            @Path("id") String id
    );


    @GET("/api/animes/{id}/roles")
    Observable<List<Role>> getRoles(
            @Path("id") String id
    );


    @GET("/api/animes/{id}/similar")
    Observable<List<SearchResult>> getSimilar(
            @Path("id") String id
    );


    @GET("/api/animes/{id}/related")
    Observable<List<Related>> getRelated(
            @Path("id") String id
    );


    @GET("/api/animes/{id}/screenshots")
    Observable<List<Screenshot>> getScreenshots(
            @Path("id") String id
    );


    @GET("/api/access_token?")
    Observable<AccessToken> getAccessToken(
            @Query("nickname") String nickname,
            @Query("password") String password
    );


    @GET("/api/users/{user_id}/anime_rates?")
    Observable<List<AnimeRate>> getAllAnimeRates(
            @Path("user_id") String userId,
            @Query("limit") int limit
    );


    /* AUTH REQUIRED */
    @GET("/api/users/whoami")
    Observable<WhoAmI> getWhoAmI();


    /* AUTH REQUIRED */
    @POST("/api/v2/user_rates")
    Observable<UserRate> getCreateUserRate(
            @Body UserRateSend userRateSend
    );


    @GET("/api/animes?")
    Observable<List<SearchResult>> getSearchByStatus(
            @Query("censored") boolean censored,
            @Query("limit") int limit,
            @Query("order") String order,
            @Query("type") String type,
            @Query("status") String status
    );

    @GET("/api/animes?")
    Observable<List<SearchResult>> getSearchByGenre(
            @Query("censored") boolean censored,
            @Query("genre") String genreId,
            @Query("limit") int limit,
            @Query("page") int page,
            @Query("season") String season,
            @Query("type") String type,
            @Query("order") String order
    );

    @GET("/api/animes?")
    Observable<List<SearchResult>> getSearchByType(
            @Query("censored") boolean censored,
            @Query("type") String type,
            @Query("limit") int limit,
            @Query("season") String season,
            @Query("order") String order
    );

    @GET("/api/animes?")
    Observable<List<SearchResult>> getRandomSearch(
            @Query("censored") boolean censored,
            @Query("type") String type,
            @Query("limit") int limit,
            @Query("page") String page,
            @Query("order") String order
    );

}
