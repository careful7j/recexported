package org.shikimori.c7j.rec.utils;

import org.shikimori.c7j.rec.AppSettings;
import org.shikimori.c7j.rec.data.Related;
import org.shikimori.c7j.rec.data.animeinfo.AnimeInfo;
import org.shikimori.c7j.rec.data.animeinfo.Genre;
import org.shikimori.c7j.rec.data.animeinfo.Video;
import org.shikimori.c7j.rec.data.roles.Role;
import org.shikimori.c7j.rec.data.search.SearchResult;
import org.shikimori.c7j.rec.network.ShikimoriApi;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("WeakerAccess")
public class SearchResultsFilter {

    public static final String GENRE_HENTAI = "Hentai";
    public static final String QUERY_STATUS_ANNOUNCED = "anons";
    public static final String QUERY_STATUS_ONGOING = "ongoing";


    public static boolean isHentai(AnimeInfo animeInfo) {
        List<Genre> genres = animeInfo.getGenres();
        for( Genre genre : genres ) {
            if ( genre.getName().equalsIgnoreCase(GENRE_HENTAI) ) {
                return true;
            }
        }
        return false;
    }


    public static String filterTextFromHtmlTags(String text) {
        String out = "";
        if (text != null) {
            char[] textChars = text.toCharArray();
            boolean ignore = false;
            for ( int charIndex = 0; charIndex < textChars.length; charIndex++ ) {
                if ( textChars[charIndex] == '[' ) {
                    ignore = true;
                    continue;
                } else if ( textChars[charIndex] == ']' ) {
                    ignore = false;
                    continue;
                }
                if (!ignore) {
                    out += textChars[charIndex];
                }
            }
        }
        return out;
    }


    public static List<SearchResult> applyAllFilters(
            AppSettings appSettings,
            List<SearchResult> searchResults,
            boolean ignoreIsOngoing
    ) {
        List<SearchResult> out = searchResults;
        if (!ignoreIsOngoing) out = filterOngoing(appSettings, out);
        return out;
    }


    public static List<SearchResult> filterOngoing(AppSettings appSettings, List<SearchResult> searchResults) {

        if ( !appSettings.readIsNotOngoing() ) {
            return searchResults;
        }
        List<SearchResult> out = new ArrayList<>();
        for (SearchResult searchResult : searchResults) {
            String status = searchResult.getStatus();
            if ( status != null ) {
                if ( !status.equals(QUERY_STATUS_ONGOING) && !status.equals((QUERY_STATUS_ANNOUNCED))) {
                    out.add(searchResult);
                }
            } else {
                out.add(searchResult);
            }
        }
        return out;
    }



    public static String getEnabledTypes(AppSettings appSettings) {
        String types = "";
        if (appSettings.readTypeMovie()) types = types + ShikimoriApi.KIND_MOVIE + ",";
        if (appSettings.readTypeOva()) types = types + ShikimoriApi.KIND_OVA + "," + ShikimoriApi.KIND_SPECIAL + ",";
        if (appSettings.readTypeTv()) types = types + ShikimoriApi.KIND_TV + ",";
        if (appSettings.readTypeOna()) types = types + ShikimoriApi.KIND_ONA + ",";
        if (types.length() > 1) types = types.substring(0, types.length() -1); //remove comma
        if (types.isEmpty()) types = ShikimoriApi.KIND_TV;
        return types;
    }


    public static String getEnabledYears(AppSettings appSettings) {
        String years = "";
        switch (appSettings.readYearFilter()) {
            case "1" : years = "1970"; break;
            case "2" : years = "2005"; break;
            case "3" : years = "2010"; break;
            case "4" : years = "2015"; break;
            default : years = "1970";
        }
        years += "_2999"; //optimistic one
        return years;
    }


    public static String getOrderByMode(AppSettings appSettings) {
        if ( appSettings.readOrderBy().equals("0") ) return ShikimoriApi.SEARCH_ORDER_BY_POPULARITY;
        if ( appSettings.readOrderBy().equals("1") ) return ShikimoriApi.SEARCH_ORDER_BY_USER_RATING;
        return ShikimoriApi.SEARCH_ORDER_BY_POPULARITY;
    }


    /** Returns the list of search suggestions */
    public static ArrayList<String> getSearchSuggestions(
            List<SearchResult> searchResults,
            String searchQuery )
    {
        ArrayList<String> out = new ArrayList<>();
        if ( searchQuery.length() > 0 && searchQuery.charAt(0) >= 'А' && searchQuery.charAt(0) <= 'я' ) {
            for ( SearchResult searchResult : searchResults ) {
                out.add(searchResult.getRussian()); //When user inputs in Russian, suggest Russian titles
            }
        } else {
            for ( SearchResult searchResult : searchResults ) {
                out.add(searchResult.getName()); //Otherwise suggest English titles
            }
        }
        return out;
    }


    /* TODO: create something more reliable */
    public static String getIdfromAnimeName(
            List<SearchResult> searchResults,
            String tName
    )
    {
        for (SearchResult searchResult : searchResults ) {
            if ( searchResult != null && tName != null ) {
                String searchResultName = searchResult.getName();
                if ( searchResultName != null && searchResultName.equals(tName) ) {
                    return searchResult.getId().toString();
                }
                String searchResultRussian = searchResult.getRussian();
                if ( searchResultRussian != null && searchResultRussian.equals(tName) ) {
                    return searchResult.getId().toString();
                }
            }
        }
        return "";
    }


    /** Server response contains not only characters, but also people. We need only characters */
    public static List<Role> filterOutPeopleFromRoles(List<Role> roles) {
        List<Role> filtered = new ArrayList<>();
        for (Role role : roles) {
            if (role.getCharacter() != null) {
                filtered.add(role);
            }
        }
        return filtered;
    }


    /** Server response contains not only promo, but also OP and ED videos. We need only promo */
    public static List<Video> filterOutPVsFromVideos(List<Video> videos) {
        List<Video> videosFiltered = new ArrayList<>();
        if (videos != null) {
            if (!videos.isEmpty()) {
                for (Video video : videos) {
                    if (video.getKind().equalsIgnoreCase("pv")) {
                        videosFiltered.add(video);
                    }
                }
            }
        }
        return videosFiltered;
    }


    /** Server response contains not only animes but also other related stuff. We need only anime */
    public static List<Related> filterOutAnimesFromRelated(List<Related> relateds) {
        List<Related> filtered = new ArrayList<>();
        for (Related related : relateds) {
            if (related.getAnime() != null) {
                filtered.add(related);
            }
        }
        return filtered;
    }

}
