package org.shikimori.c7j.rec.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.AppSettings;
import org.shikimori.c7j.rec.CoreActivity;
import org.shikimori.c7j.rec.data.Related;
import org.shikimori.c7j.rec.data.Screenshot;
import org.shikimori.c7j.rec.data.animeinfo.Video;
import org.shikimori.c7j.rec.ui.AdapterRelated;
import org.shikimori.c7j.rec.ui.AdapterScreenshots;
import org.shikimori.c7j.rec.ui.AdapterTrailers;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.utils.L;
import org.shikimori.c7j.rec.utils.ResponseInfo;
import org.shikimori.c7j.rec.utils.SearchResultsFilter;
import org.shikimori.c7j.rec.data.animeinfo.AnimeInfo;
import org.shikimori.c7j.rec.data.animeinfo.Genre;
import org.shikimori.c7j.rec.data.animeinfo.Studio;
import org.shikimori.c7j.rec.data.animerates.AnimeRate;
import org.shikimori.c7j.rec.data.roles.Role;
import org.shikimori.c7j.rec.network.ShikimoriService;
import org.shikimori.c7j.rec.ui.AdapterCharacters;
import org.shikimori.c7j.rec.ui.AppCompatFontButton;
import org.shikimori.c7j.rec.ui.FontTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AnimeInfoFragment extends ABaseFragment {

    @BindView(R.id.animeinfo_group) View animeInfoGroup;

    @BindView(R.id.tv_header) FontTextView header;
    @BindView(R.id.tv_subHeader) FontTextView subHeader;
    @BindView(R.id.tv_type) FontTextView type;
    @BindView(R.id.tv_studio) FontTextView studio;
    @BindView(R.id.tv_duration) FontTextView duration;
    @BindView(R.id.tv_status) FontTextView status;
    @BindView(R.id.tv_rating) FontTextView rating;
    @BindView(R.id.tv_scores) FontTextView scores;
    @BindView(R.id.tv_genres) FontTextView genres;
    @BindView(R.id.tv_description) FontTextView description;

    @BindView(R.id.loading_circle) View loadingCircle;

    @BindView(R.id.rv_characters) RecyclerView rvCharacters;
    @BindView(R.id.rv_trailers) RecyclerView rvTrailers;
    @BindView(R.id.rv_related) RecyclerView rvRelated;
    @BindView(R.id.rv_screenshots) RecyclerView rvScreenshots;

    @BindView(R.id.btn_save) public AppCompatFontButton btnSave;
    @BindView(R.id.tv_done) public AppCompatFontButton btnDone;
    @BindView(R.id.loading_circle_btn) public ProgressBar loadingCircleBtn;

    @BindView(R.id.restricted_layout) View restrictedLayout;

    @BindView(R.id.cardview_characters) View cardViewCharacters;
    @BindView(R.id.cardview_trailers) View cardViewTrailers;
    @BindView(R.id.cardview_related) View cardViewRelated;
    @BindView(R.id.cardview_bottom_pane) View cardViewBottomPane;

    @BindString(R.string.frag_animeinfo_duration) String strDuration;
    @BindString(R.string.frag_animeinfo_duration_short) String strDurationShort;
    @BindString(R.string.frag_animeinfo_status_released) String strReleased;
    @BindString(R.string.frag_animeinfo_status_ongoing) String strOngoing;
    @BindString(R.string.frag_animeinfo_status_announced) String strAnnounced;
    @BindString(R.string.frag_animeinfo_first_episode) String strFirstEpisode;
    @BindString(R.string.frag_animeinfo_type) String strType;
    @BindString(R.string.frag_animeinfo_studio) String strStudio;
    @BindString(R.string.frag_animeinfo_status) String strStatus;
    @BindString(R.string.frag_animeinfo_agerating) String strAgeRating;
    @BindString(R.string.frag_animeinfo_scores) String strScores;
    @BindString(R.string.frag_animeinfo_genres) String strGenres;
    @BindString(R.string.frag_animeinfo_no_description) String strNoDescription;

    @BindString(R.string.frag_animeinfo_promo_video) String strPromoVideo;

    @BindString(R.string.frag_animeinfo_type_tv) String strTypeTv;
    @BindString(R.string.frag_animeinfo_type_ova) String strTypeOva;
    @BindString(R.string.frag_animeinfo_type_ona) String strTypeOna;
    @BindString(R.string.frag_animeinfo_type_special) String strTypeSpecial;
    @BindString(R.string.frag_animeinfo_type_movie) String strTypeMovie;

    @BindString(R.string.frag_animeinfo_rating_pg13) String strRatingPG13;
    @BindString(R.string.frag_animeinfo_rating_g) String strRatingG;
    @BindString(R.string.frag_animeinfo_rating_nc17) String strRatingNC17;
    @BindString(R.string.frag_animeinfo_rating_pg) String strRatingPG;
    @BindString(R.string.frag_animeinfo_rating_r) String strRatingR;
    @BindString(R.string.frag_animeinfo_rating_rplus) String strRatingRPlus;
    @BindString(R.string.frag_animeinfo_rating_rx) String strRatingRX;

    @BindString(R.string.frag_animeinfo_status_planned) public String strStatusPlanned;
    @BindString(R.string.frag_animeinfo_status_completed) String strStatusCompleted;
    @BindString(R.string.frag_animeinfo_status_onhold) String strStatusOnHold;
    @BindString(R.string.frag_animeinfo_status_dropped) String strStatusDropped;
    @BindString(R.string.frag_animeinfo_status_watching) String strStatusWatching;
    @BindString(R.string.frag_animeinfo_status_rewatching) String strStatusRewatching;

    public static final String QUERY_STATUS_RELEASED = "released";
    public static final String QUERY_STATUS_ANNOUNCED = "anons";
    public static final String QUERY_STATUS_ONGOING = "ongoing";

    public static final String QUERY_STATUS_PLANNED = "planned";
    public static final String QUERY_STATUS_COMPLETED = "completed";
    public static final String QUERY_STATUS_ON_HOLD = "on_hold";
    public static final String QUERY_STATUS_DROPPED = "dropped";
    public static final String QUERY_STATUS_WATCHING = "watching";
    public static final String QUERY_STATUS_REWATCHING = "rewatching";

    public static final String QUERY_TYPE_TV = "tv";
    public static final String QUERY_TYPE_OVA = "ova";
    public static final String QUERY_TYPE_ONA = "ona";
    public static final String QUERY_TYPE_SPECIAL = "special";
    public static final String QUERY_TYPE_MOVIE = "movie";

    public static final String QUERY_RATING_PG13 = "pg_13";
    public static final String QUERY_RATING_R = "r";
    public static final String QUERY_RATING_RPLUS = "r_plus";
    public static final String QUERY_RATING_NC17 = "nc_17";
    public static final String QUERY_RATING_PG = "pg";
    public static final String QUERY_RATING_G = "g";
    public static final String QUERY_RATING_RX = "rx";

    public static final String STR_UNKNOWN = "?";

    public boolean useEnglish = false; // If parse English strings from server response instead of Russian

    private View holder;

    private AdapterCharacters rvAdapterCharacters;
    private AdapterTrailers rvAdapterTrailers;
    private AdapterRelated rvAdapterRelated;
    private AdapterScreenshots rvAdapterScreenshots;
    private ImageView poster;

    private AnimeInfo animeInfo = null;
    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.ANIME_INFO);

    public FragmentInfo getFragmentInfo() { return fragmentInfo; }
    public AnimeInfo getAnimeInfo() { return animeInfo; }


    @OnClick(R.id.btn_suggest) void btnSuggest() {
        if ( activitySearch.isInternetConnected()) {
            if (animeInfo != null ) {
                activitySearch.getSearchView().setQuery("", false);
                activitySearch.getRm().requestSimilar((ActivitySearch) getActivity(), animeInfo.getId().toString(), false);
                GAnalytics.reportSuggest(animeInfo.getRussian());
            }
        }
    }


    @OnClick(R.id.btn_toshikimori) void btnToShikimori() {
        if ( activitySearch.isInternetConnected()) {
            GAnalytics.reportToShikimori(animeInfo.getRussian());
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(ShikimoriService.WEBSERVICE_BASE_URL + animeInfo.getUrl() + "?utm_source=" + getString(R.string.shikimori_app_id))));
        }
    }


    @OnClick(R.id.btn_save) void btnSaveClicked() {
        activitySearch.onSavePressed(animeInfo.getId());
    }


    @OnClick(R.id.btn_back) void btnReturnBack() {
        activitySearch.onBackPressed();
    }


    @SuppressWarnings("unchecked")
    public void updateData(FragmentInfo tempFragmentInfo) {
        if ( tempFragmentInfo.getFragmentType() == FragmentInfo.ANIME_INFO ) {
            List<ResponseInfo> responses = tempFragmentInfo.getResponses();
            for ( ResponseInfo responseInfo : responses ) {
                if ( responseInfo.getResponseType() == ResponseInfo.GET_ANIMEINFO ) {
                    updateData( (AnimeInfo) responseInfo.getApiResponse() );
                }
                if ( responseInfo.getResponseType() == ResponseInfo.GET_CHARACTERS_LIST ) {
                    updateCharacters( (List<Role>) responseInfo.getApiResponse() );
                }
                if ( responseInfo.getResponseType() == ResponseInfo.GET_RELATED_LIST ) {
                    updateRelated( (List<Related>) responseInfo.getApiResponse() );
                }
                if ( responseInfo.getResponseType() == ResponseInfo.GET_SCREENSHOTS ) {
                    updateScreenshots( (List<Screenshot>) responseInfo.getApiResponse() );
                }
            }
        }
    }


    private void updateHeaderAndSubheader() {
        if ( useEnglish ) {
            if ( animeInfo.getEnglish() != null && header != null ) {
                if ( animeInfo.getEnglish().size() > 0 && animeInfo.getEnglish().get(0) != null ) {
                    header.setText(animeInfo.getEnglish().get(0));
                } else {
                    if ( animeInfo.getName() != null ) {
                        header.setText(animeInfo.getName());
                    }
                }
            }
        } else {
            if ( animeInfo.getRussian() != null && header != null ) {
                header.setText(animeInfo.getRussian());
            }
        }
        if ( animeInfo.getName() != null && subHeader != null) {
            subHeader.setText(animeInfo.getName());
        }
    }


    private void updatePoster() {
        if (holder != null) {
            poster = (ImageView) holder.findViewById(R.id.img_anime_poster);
            if ( animeInfo.getImage() != null ) {
                Glide.with(this)
                        .load(ShikimoriService.WEBSERVICE_BASE_URL + animeInfo.getImage().getOriginal())
                        .centerCrop()
                        .into(poster);
            }
        }
    }


    private void updateStudio() {
        String studioTxt = strStudio;
        List<Studio> studios = animeInfo.getStudios();
        L.t("STUDIO: " + animeInfo.getStudios());

        if ( studios != null ) {
            for ( Studio tStudio : studios ) {
                studioTxt += tStudio.getName() + ", ";
            }
            if ( !studioTxt.isEmpty() && studios.size() > 0 ) {
                studioTxt = String.copyValueOf(studioTxt.toCharArray(), 0, studioTxt.length() - 2);
            } else {
                studio.setVisibility(View.GONE);
            }
        }
        if (studio != null) studio.setText(studioTxt);
    }


    private void updateStatus() {
        String statusTxt = "";
        String airedOn = animeInfo.getAiredOn();
        boolean airedOnNotNull = (airedOn != null && !airedOn.equals("null"));

        switch (animeInfo.getStatus()) {

            case QUERY_STATUS_RELEASED :
                if (airedOnNotNull) {
                    statusTxt = strReleased +
                            " (" + String.copyValueOf(airedOn.toCharArray(), 0, 4 ) + ")";
                } else statusTxt = strReleased;
                break;


            case QUERY_STATUS_ONGOING :
                statusTxt = strOngoing + " " +
                        animeInfo.getEpisodesAired() + "/";
                Integer episodesMax = animeInfo.getEpisodes();
                if (episodesMax == 0) statusTxt += "?"; else statusTxt += episodesMax;

                if (airedOnNotNull) {
                    statusTxt = statusTxt + " \n" + strFirstEpisode + animeInfo.getAiredOn();
                }
                break;


            case QUERY_STATUS_ANNOUNCED :
                statusTxt = strAnnounced;
                try {
                    if (airedOnNotNull) {
                        statusTxt = strAnnounced + " (" + animeInfo.getAiredOn().substring( 0, 10 ) + ")";
                        if ( animeInfo.getAiredOn().contains("01-01") ) {
                            statusTxt = strAnnounced + " (" + animeInfo.getAiredOn().substring( 0, 4 ) + ")";
                        }
                    }
                } catch (Exception e) { /* Don't care */}
                break;

        }
        if (status != null) status.setText( strStatus + statusTxt );
    }


    private void updateGenres() {
        String genresTxt = "";
        List<Genre> tGenres = animeInfo.getGenres();
        if ( tGenres != null ) {

            for ( Genre genre : tGenres ) {
                if (useEnglish) {
                    genresTxt += genre.getName() + ", ";
                } else genresTxt += genre.getRussian() + ", ";
            }

            if ( !genresTxt.isEmpty() ) {
                genresTxt = String.copyValueOf(genresTxt.toCharArray(), 0, genresTxt.length() - 2); //remove comma
            }
        }
        if (genres != null) genres.setText( strGenres + genresTxt );
    }


    private void checkForHentai() {
        if ( SearchResultsFilter.isHentai( animeInfo ) ) {
            if (restrictedLayout != null) {
                restrictedLayout.setVisibility(View.VISIBLE);
                loadingCircle.setVisibility(View.INVISIBLE);
                cardViewBottomPane.setVisibility(View.INVISIBLE);
            }
        } else { makeVisible(); }
    }


    private void updateDurationDescriptionAndScores() {
        if (duration != null) {
            if (animeInfo.getDuration() == 0 && animeInfo.getEpisodes() == 0) {
                duration.setVisibility(View.GONE);
            } else {
                if ( animeInfo.getDuration() != 0  ) {
                    duration.setText( String.format(
                            strDuration,
                            animeInfo.getEpisodes() == 0 ? STR_UNKNOWN : animeInfo.getEpisodes().toString(),
                            animeInfo.getDuration().toString()
                    ));
                } else {
                    duration.setText( String.format(
                            strDurationShort,
                            getResources().getQuantityString(
                                    R.plurals.plurals_episodes, animeInfo.getEpisodes(), animeInfo.getEpisodes())
                    ));
                }

            }
        }

        if (scores != null) {
            if (animeInfo.getScore().equals("0.0")) {
                scores.setVisibility(View.GONE);
            } else { scores.setText(strScores + " " + animeInfo.getScore()); }
        }

        if (description != null) {
            if (useEnglish) {
                description.setText(strNoDescription);
            } else {
                String descriptionTxt = SearchResultsFilter.filterTextFromHtmlTags( animeInfo.getDescription());
                if (!descriptionTxt.isEmpty()) {
                    description.setText(SearchResultsFilter.filterTextFromHtmlTags( animeInfo.getDescription()) );
                } else {
                    description.setText(strNoDescription);
                }
            }
        }
    }


    private void updateRating() {
        String strRating = animeInfo.getRating();
        switch (strRating) {

            case QUERY_RATING_PG13 :
                strRating = strRatingPG13;
                break;

            case QUERY_RATING_RPLUS :
                strRating = strRatingRPlus;
                break;

            case QUERY_RATING_R :
                strRating = strRatingR;
                break;

            case QUERY_RATING_NC17 :
                strRating = strRatingNC17;
                break;

            case QUERY_RATING_PG :
                strRating = strRatingPG;
                break;

            case QUERY_RATING_G :
                strRating = strRatingG;
                break;

            case QUERY_RATING_RX :
                strRating = strRatingRX;
                break;

            default : strRating = strRating.toUpperCase();
        }
        if (rating == null) return;
        if (strRating.equalsIgnoreCase("NONE")) {
            rating.setVisibility(View.GONE  );
        } else {
            rating.setText( strAgeRating + strRating );
        }
    }


    private void updateAnimeType() {
        String typeTxt = animeInfo.getKind();
        switch (animeInfo.getKind()) {

            case QUERY_TYPE_TV :
                typeTxt = strTypeTv;
                break;

            case QUERY_TYPE_OVA :
                typeTxt = strTypeOva;
                break;

            case QUERY_TYPE_ONA :
                typeTxt = strTypeOna;
                break;

            case QUERY_TYPE_SPECIAL :
                typeTxt = strTypeSpecial;
                break;

            case QUERY_TYPE_MOVIE :
                typeTxt = strTypeMovie;
                break;
        }
        if (type != null && typeTxt != null) type.setText(strType + typeTxt);
    }


    public void updateSaveButton() {
        if (activitySearch == null) return;
        if (activitySearch.animeRates != null) {

            AnimeRate tAnimeRate = null;
            int animeInfoAnimeId = animeInfo.getId();
            for (AnimeRate animeRate : activitySearch.animeRates) {
                int animeRateIdAnime = animeRate.getAnime().getId();
                if (animeInfoAnimeId == animeRateIdAnime) {
                    tAnimeRate = animeRate;
                }
            }
            if (tAnimeRate != null) {
                if (loadingCircleBtn != null) {
                    loadingCircleBtn.setVisibility(View.GONE);
                    btnSave.setVisibility(View.GONE);
                    btnDone.setVisibility(View.VISIBLE);
                }
                switch (tAnimeRate.getStatus()) {

                    case QUERY_STATUS_COMPLETED :
                        btnDone.setText(strStatusCompleted);
                        break;

                    case QUERY_STATUS_ON_HOLD :
                        btnDone.setText(strStatusOnHold);
                        break;

                    case QUERY_STATUS_PLANNED :
                        btnDone.setText(strStatusPlanned);
                        break;

                    case QUERY_STATUS_REWATCHING :
                        btnDone.setText(strStatusRewatching);
                        break;

                    case QUERY_STATUS_WATCHING :
                        btnDone.setText(strStatusWatching);
                        break;

                    case QUERY_STATUS_DROPPED :
                        btnDone.setText(strStatusDropped);
                        btnDone.setTextColor(Color.RED);
                        break;

                    default : btnDone.setText("");
                }
            }
        }
    }


    public void updateData(AnimeInfo animeInfo) {
        this.animeInfo = animeInfo;
        if (!CoreActivity.locale.equals(CoreActivity.LOCALE_RUSSIAN)) { useEnglish = true; }
        updateHeaderAndSubheader();
        updatePoster();
        updateAnimeType();
        updateStudio();
        updateStatus();
        updateRating();
        updateGenres();
        updateDurationDescriptionAndScores();
        updateTrailers(animeInfo.getVideos());
        updateSaveButton();
        checkForHentai();
    }


    public void updateCharacters(List<Role> roles) {
        List<Role> filtered = SearchResultsFilter.filterOutPeopleFromRoles(roles);
        if (filtered.isEmpty()) {
            cardViewCharacters.setVisibility(View.GONE);
        } else if ( rvAdapterCharacters != null ) rvAdapterCharacters.updateDataSet(filtered);
    }


    public void updateTrailers(List<Video> videos) {
        List<Video> filtered = SearchResultsFilter.filterOutPVsFromVideos(videos);
        if (filtered.isEmpty()) {
            if (cardViewTrailers != null) {
                cardViewTrailers.setVisibility(View.GONE);
            }
        } else if ( rvAdapterTrailers != null ) rvAdapterTrailers.updateDataSet(filtered);
    }


    public void updateRelated(List<Related> relateds) {
        List<Related> filtered = SearchResultsFilter.filterOutAnimesFromRelated(relateds);
        if (filtered.isEmpty()) {
            cardViewRelated.setVisibility(View.GONE);
        } else if ( rvAdapterRelated != null ) rvAdapterRelated.updateDataSet(filtered);
    }


    public void updateScreenshots(List<Screenshot> screenshots) {
        AppSettings appSettings = null;
        if ( activitySearch != null ) {
            appSettings = activitySearch.getAppSettings();
        }
        if ( rvAdapterScreenshots != null ) rvAdapterScreenshots.updateDataSet(screenshots, appSettings, getActivity());
    }


    private void makeVisible() {
        if (loadingCircle != null) {
            cardViewBottomPane.setVisibility(View.VISIBLE);
            loadingCircle.setVisibility(View.INVISIBLE);
            animeInfoGroup.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        holder = inflater.inflate(R.layout.fragment_animeinfo, container, false);
        unbinder = ButterKnife.bind(this, holder);

        rvCharacters.setHasFixedSize(true);
        rvAdapterCharacters = new AdapterCharacters(new ArrayList<Role>());
        rvCharacters.setAdapter(rvAdapterCharacters);
        rvCharacters.setNestedScrollingEnabled(false);

        rvTrailers.setHasFixedSize(true);
        rvAdapterTrailers = new AdapterTrailers(new ArrayList<Video>(), strPromoVideo);
        rvTrailers.setAdapter(rvAdapterTrailers);
        rvTrailers.setNestedScrollingEnabled(false);

        rvRelated.setHasFixedSize(true);
        rvRelated.setLayoutManager(new LinearLayoutManager(activitySearch));
        rvAdapterRelated = new AdapterRelated(new ArrayList<Related>(), this);
        rvRelated.setAdapter(rvAdapterRelated);
        rvRelated.setNestedScrollingEnabled(false);

        rvScreenshots.setHasFixedSize(true);
        rvScreenshots.setLayoutManager(new LinearLayoutManager(activitySearch));
        rvAdapterScreenshots = new AdapterScreenshots(new ArrayList<Screenshot>());
        rvScreenshots.setAdapter(rvAdapterScreenshots);
        rvScreenshots.setNestedScrollingEnabled(false);

        return holder;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (animeInfoGroup != null) {
                    animeInfoGroup.requestFocus(); //hide imei-keyboard
                    animeInfoGroup.scrollTo(0, 0);
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        if (rvAdapterRelated != null) rvAdapterRelated.onDestroy();
        super.onDestroyView();
    }
}
