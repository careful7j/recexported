package org.shikimori.c7j.rec.data.userrate;


import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class UserRateSend {

    @SerializedName("user_id")
    private Integer userId;
    @SerializedName("target_id")
    private Integer targetId;
    @SerializedName("target_type")
    private String targetType;
    private Integer score;
    private String status;
    private Integer rewatches;
    private Integer episodes;
    private Integer volumes;
    private Integer chapters;
    private String text;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRewatches() {
        return rewatches;
    }

    public void setRewatches(Integer rewatches) {
        this.rewatches = rewatches;
    }

    public Integer getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Integer episodes) {
        this.episodes = episodes;
    }

    public Integer getVolumes() {
        return volumes;
    }

    public void setVolumes(Integer volumes) {
        this.volumes = volumes;
    }

    public Integer getChapters() {
        return chapters;
    }

    public void setChapters(Integer chapters) {
        this.chapters = chapters;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toGsonString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public UserRateSend fromGsonString(String string) {
        Gson gson = new Gson();
        return gson.fromJson(string, UserRateSend.class);
    }
}
