package org.shikimori.c7j.rec.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.shikimori.c7j.rec.AppSettings;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.network.ShikimoriApi;
import org.shikimori.c7j.rec.network.ShikimoriService;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.ui.UIUtils;
import org.shikimori.c7j.rec.utils.FragmentInfo;

import java.util.HashMap;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author c7j at 20.06.2017
 */

public class TabHolderFragment extends ABaseFragment {


    @BindView(R.id.layout_searchpage) ScrollView searchPage;
    @BindView(R.id.layout_genrepickerpage) public ScrollView genrePickerPage;

    @BindView(R.id.btn_pick_genre1) TextView btnGenre1;
    @BindView(R.id.btn_pick_genre2) TextView btnGenre2;


    @BindString(R.string.frag_offer_action) String strGAction;
    @BindString(R.string.frag_offer_adventure) String strGAdventure;
    @BindString(R.string.frag_offer_cars) String strGCars;
    @BindString(R.string.frag_offer_comedy) String strGComedy;
    @BindString(R.string.frag_offer_dementia) String strGDementia;
    @BindString(R.string.frag_offer_demons) String strGDemons;
    @BindString(R.string.frag_offer_drama) String strGDrama;
    @BindString(R.string.frag_offer_ecchi) String strGEcchi;
    @BindString(R.string.frag_offer_fantasy) String strGFantasy;
    @BindString(R.string.frag_offer_game) String strGGame;
    @BindString(R.string.frag_offer_harem) String strGHarem;
    @BindString(R.string.frag_offer_historical) String strGHistorical;
    @BindString(R.string.frag_offer_horror) String strGHorror;
    @BindString(R.string.frag_offer_josei) String strGJosei;
    @BindString(R.string.frag_offer_kids) String strGKids;
    @BindString(R.string.frag_offer_magic) String strGMagic;
    @BindString(R.string.frag_offer_martial_arts) String strGMatrialArts;
    @BindString(R.string.frag_offer_mecha) String strGMecha;
    @BindString(R.string.frag_offer_military) String strGMilitary;
    @BindString(R.string.frag_offer_music) String strGMusic;
    @BindString(R.string.frag_offer_mystery) String strGMystery;
    @BindString(R.string.frag_offer_parody) String strGParody;
    @BindString(R.string.frag_offer_police) String strGPolice;
    @BindString(R.string.frag_offer_psychological) String strGPsychological;
    @BindString(R.string.frag_offer_romance) String strGRomance;
    @BindString(R.string.frag_offer_samurai) String strGSamurai;
    @BindString(R.string.frag_offer_school) String strGSchool;
    @BindString(R.string.frag_offer_sci_fi) String strGSciFi;
    @BindString(R.string.frag_offer_seinen) String strGSeinen;
    @BindString(R.string.frag_offer_shoujo) String strGShoujo;
    @BindString(R.string.frag_offer_shoujo_ai) String strGShoujoAi;
    @BindString(R.string.frag_offer_shounen) String strGShounen;
    @BindString(R.string.frag_offer_shounen_ai) String strGShounenAi;
    @BindString(R.string.frag_offer_slice_of_life) String strGSliceOfLife;
    @BindString(R.string.frag_offer_space) String strGSpace;
    @BindString(R.string.frag_offer_sports) String strGSports;
    @BindString(R.string.frag_offer_super_power) String strGSuperPower;
    @BindString(R.string.frag_offer_supernatural) String strGSuperNatural;
    @BindString(R.string.frag_offer_thriller) String strGThriller;
    @BindString(R.string.frag_offer_vampire) String strGVampire;


    @BindString(R.string.frag_tabholder_header_search) String strHeaderSearch;
    @BindString(R.string.frag_tabholder_header_menu) String strHeaderMenu;
    @BindString(R.string.frag_offer_select_genre_button) String strSelect;


    public static final String ARTIST_PAGE_URL = "https://vk.com/artsbymikuo";
    public static final String SHIKIMORI_CLUB_URL = "https://shikimori.org/clubs/903-klub-mobilnogo-prilozheniya-anime-rekomendatsii";
    public static final String SHIKIMORI_ANIME_NEWS = "https://shikimori.org/forum/news";

    public static final String EMPTY_GENRE = "0";

    public static final String GENRE_ACTION = "1";
    public static final String GENRE_ADVENTURE = "2";
    public static final String GENRE_CARS = "3";
    public static final String GENRE_COMEDY = "4";
    public static final String GENRE_DEMENTIA = "5";

    public static final String GENRE_DEMONS = "6";
    public static final String GENRE_MYSTERY = "7";
    public static final String GENRE_DRAMA = "8";
    public static final String GENRE_ECCHI = "9";
    public static final String GENRE_FANTASY = "10";

    public static final String GENRE_GAME = "11";
    public static final String GENRE_HAREM = "35";
    public static final String GENRE_HISTORICAL = "13";
    public static final String GENRE_HORROR = "14";
    public static final String GENRE_JOSEI = "43";
    public static final String GENRE_KIDS = "15";

    public static final String GENRE_MAGIC = "16";
    public static final String GENRE_MARTIAL_ARTS = "17";
    public static final String GENRE_MECHA = "18";
    public static final String GENRE_MILITARY = "38";
    public static final String GENRE_MUSIC = "19";

    public static final String GENRE_PARODY = "20";
    public static final String GENRE_POLICE = "39";
    public static final String GENRE_SAMURAI = "21";
    public static final String GENRE_ROMANCE = "22";
    public static final String GENRE_SCHOOL = "23";

    public static final String GENRE_SCI_FI = "24";
    public static final String GENRE_SHOUJO = "25";
    public static final String GENRE_SHOUJO_AI = "26";
    public static final String GENRE_SHOUNEN = "27";
    public static final String GENRE_SHOUNEN_AI = "28";

    public static final String GENRE_SPACE = "29";
    public static final String GENRE_SPORTS = "30";
    public static final String GENRE_SUPER_POWER = "31";
    public static final String GENRE_VAMPIRES = "32";
    public static final String GENRE_SLICE_OF_LIFE = "36";

    public static final String GENRE_SUPERNATURAL = "37";
    public static final String GENRE_PSYCHOLOGICAL = "40";
    public static final String GENRE_THRILLER = "41";
    public static final String GENRE_SEINEN = "42";

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.TABHOLDER);
    public FragmentInfo getFragmentInfo() { return fragmentInfo; }

    private AppSettings appSettings = null;

    private static String genre1 = EMPTY_GENRE;
    private static String genre2 = EMPTY_GENRE;
    private boolean isSelectingGenre1 = false;
    private HashMap<String, String> genresIds = new HashMap<>();



    @OnClick(R.id.btn_search) void btnSearchClicked() {
        if ( !( genre1.equals(EMPTY_GENRE) && genre2.equals(EMPTY_GENRE) ) ) {
            if (genre1.equals(genre2)) {
                activitySearch.getRm().requestSearchByGenre( activitySearch, genre1 );
            } else {
                String genresParameter = "";
                if (!genre1.equals(EMPTY_GENRE)) {  genresParameter = genre1 + ",";  }
                if (!genre2.equals(EMPTY_GENRE)) {  genresParameter += genre2;  }
                activitySearch.getRm().requestSearchByGenre( activitySearch, genresParameter );
            }
        } else {
            UIUtils.showCustomToast(getActivity(), R.layout.toast_pick_genre_please, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.btn_advanced_settings) void btnAdvancedSettingsClicked() {
        activitySearch.loadPrefsFragment(null);
    }


    @OnClick(R.id.btn_anime_news) void btnAnimeNewsClicked() {
        GAnalytics.reportAnimeNewsVisited();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SHIKIMORI_ANIME_NEWS)));
    }


    @OnClick(R.id.btn_my_animelist) void btnMyAnimelistClicked() {
        tryToShowAnimeList();
    }


    @OnClick(R.id.btn_to_shikimori_club) void btnToShikimoriClubClicked() {
        GAnalytics.reportShikimoriClubVisited();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SHIKIMORI_CLUB_URL)));
    }


    @OnClick(R.id.btn_rate_us) void btnRateUsClicked() {
        GAnalytics.reportRateUs();
        UIUtils.openAppGooglePlayMarketPage(activitySearch);
    }


    @OnClick(R.id.btn_artist_page) void btnArtistVkClicked() {
        GAnalytics.reportAuthorFromStartingFragmentVisited(GAnalytics.ACTION_FROM_SETTINGS_FRAGMENT);
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ARTIST_PAGE_URL)));
    }


    @OnClick(R.id.btn_pick_genre1) void btnPickGenre1Clicked() {
        searchPage.setVisibility(View.GONE);
        genrePickerPage.setVisibility(View.VISIBLE);
        isSelectingGenre1 = true;
    }


    @OnClick(R.id.btn_pick_genre2) void btnPickGenre2Clicked() {
        searchPage.setVisibility(View.GONE);
        genrePickerPage.setVisibility(View.VISIBLE);
        isSelectingGenre1 = false;
    }


    @OnClick(R.id.btn_cancel) public void btnCancelClicked() {
        searchPage.setVisibility(View.VISIBLE);
        genrePickerPage.setVisibility(View.GONE);
    }


    @OnClick({
            R.id.btn_offer_airing,
            R.id.btn_offer_anime_movies,
            R.id.btn_offer_announced,
            R.id.btn_offer_latest,
            R.id.btn_offer_random
    }) void search(View v) {

        switch( v.getId() ) {

            case R.id.btn_offer_airing :
                activitySearch.getRm().requestSearchByStatus( activitySearch, ShikimoriApi.SEARCH_STATUS_ONGOING );
                GAnalytics.reportMenuItemSelected(GAnalytics.ACTION_ITEM_AIRING);
                break;

            case R.id.btn_offer_latest :
                activitySearch.getRm().requestSearchByStatus( activitySearch, ShikimoriApi.SEARCH_STATUS_LATEST );
                GAnalytics.reportMenuItemSelected(GAnalytics.ACTION_ITEM_LATEST);
                break;

            case R.id.btn_offer_announced :
                activitySearch.getRm().requestSearchByStatus( activitySearch, ShikimoriApi.SEARCH_STATUS_ANNOUNCED );
                GAnalytics.reportMenuItemSelected(GAnalytics.ACTION_ITEM_ANNOUNCED);
                break;

            case R.id.btn_offer_anime_movies :
                activitySearch.getRm().requestSearchByType( activitySearch, ShikimoriApi.SEARCH_TYPE_MOVIE );
                GAnalytics.reportMenuItemSelected(GAnalytics.ACTION_ITEM_ANIME_MOVIES);
                break;

            case R.id.btn_offer_random :
                activitySearch.getRm().requestSearchRandom( activitySearch );
                GAnalytics.reportMenuItemSelected(GAnalytics.ACTION_ITEM_RANDOM);
                break;
        }
    }


    @OnClick({
            R.id.btn_offer_action,
            R.id.btn_offer_adventure,
            R.id.btn_offer_cars,
            R.id.btn_offer_comedy,
            R.id.btn_offer_dementia,

            R.id.btn_offer_demons,
            R.id.btn_offer_mystery,
            R.id.btn_offer_drama,
            R.id.btn_offer_ecchi,
            R.id.btn_offer_fantasy,

            R.id.btn_offer_game,
            R.id.btn_offer_harem,
            R.id.btn_offer_historical,
            R.id.btn_offer_horror,
            R.id.btn_offer_josei,

            R.id.btn_offer_kids,
            R.id.btn_offer_magic,
            R.id.btn_offer_martial_arts,
            R.id.btn_offer_mecha,
            R.id.btn_offer_military,

            R.id.btn_offer_music,
            R.id.btn_offer_parody,
            R.id.btn_offer_police,
            R.id.btn_offer_samurai,
            R.id.btn_offer_romance,

            R.id.btn_offer_school,
            R.id.btn_offer_sci_fi,
            R.id.btn_offer_shoujo,
            R.id.btn_offer_shoujo_ai,
            R.id.btn_offer_shounen,

            R.id.btn_offer_shounen_ai,
            R.id.btn_offer_space,
            R.id.btn_offer_sports,
            R.id.btn_offer_super_power,
            R.id.btn_offer_vampires,

            R.id.btn_offer_slice_of_life,
            R.id.btn_offer_supernatural,
            R.id.btn_offer_psychological,
            R.id.btn_offer_super_thriller,
            R.id.btn_offer_seinen,

            R.id.btn_clear
    })

    public void genrePicked(View v) {

        String newGenre = "0";
        switch( v.getId() ) {

            case R.id.btn_offer_action : newGenre = GENRE_ACTION; break;
            case R.id.btn_offer_adventure : newGenre = GENRE_ADVENTURE; break;
            case R.id.btn_offer_cars : newGenre = GENRE_CARS; break;
            case R.id.btn_offer_comedy : newGenre = GENRE_COMEDY; break;
            case R.id.btn_offer_dementia : newGenre = GENRE_DEMENTIA; break;

            case R.id.btn_offer_demons : newGenre = GENRE_DEMONS; break;
            case R.id.btn_offer_mystery : newGenre = GENRE_MYSTERY; break;
            case R.id.btn_offer_drama : newGenre = GENRE_DRAMA; break;
            case R.id.btn_offer_ecchi : newGenre = GENRE_ECCHI; break;
            case R.id.btn_offer_fantasy : newGenre = GENRE_FANTASY; break;

            case R.id.btn_offer_game : newGenre = GENRE_GAME; break;
            case R.id.btn_offer_harem : newGenre = GENRE_HAREM; break;
            case R.id.btn_offer_historical : newGenre = GENRE_HISTORICAL; break;
            case R.id.btn_offer_horror : newGenre = GENRE_HORROR; break;
            case R.id.btn_offer_josei : newGenre = GENRE_JOSEI; break;

            case R.id.btn_offer_kids : newGenre = GENRE_KIDS; break;
            case R.id.btn_offer_magic : newGenre = GENRE_MAGIC; break;
            case R.id.btn_offer_martial_arts : newGenre = GENRE_MARTIAL_ARTS; break;
            case R.id.btn_offer_mecha : newGenre = GENRE_MECHA; break;
            case R.id.btn_offer_military : newGenre = GENRE_MILITARY; break;

            case R.id.btn_offer_music : newGenre = GENRE_MUSIC; break;
            case R.id.btn_offer_parody : newGenre = GENRE_PARODY; break;
            case R.id.btn_offer_police : newGenre = GENRE_POLICE; break;
            case R.id.btn_offer_samurai : newGenre = GENRE_SAMURAI; break;
            case R.id.btn_offer_romance : newGenre = GENRE_ROMANCE; break;

            case R.id.btn_offer_school : newGenre = GENRE_SCHOOL; break;
            case R.id.btn_offer_sci_fi : newGenre = GENRE_SCI_FI; break;
            case R.id.btn_offer_shoujo : newGenre = GENRE_SHOUJO; break;
            case R.id.btn_offer_shoujo_ai : newGenre = GENRE_SHOUJO_AI; break;
            case R.id.btn_offer_shounen : newGenre = GENRE_SHOUNEN; break;

            case R.id.btn_offer_shounen_ai : newGenre = GENRE_SHOUNEN_AI; break;
            case R.id.btn_offer_space : newGenre = GENRE_SPACE; break;
            case R.id.btn_offer_sports : newGenre = GENRE_SPORTS; break;
            case R.id.btn_offer_super_power : newGenre = GENRE_SUPER_POWER; break;
            case R.id.btn_offer_vampires : newGenre = GENRE_VAMPIRES; break;

            case R.id.btn_offer_slice_of_life : newGenre = GENRE_SLICE_OF_LIFE; break;
            case R.id.btn_offer_supernatural : newGenre = GENRE_SUPERNATURAL; break;
            case R.id.btn_offer_psychological : newGenre = GENRE_PSYCHOLOGICAL; break;
            case R.id.btn_offer_super_thriller : newGenre = GENRE_THRILLER; break;
            case R.id.btn_offer_seinen : newGenre = GENRE_SEINEN; break;

            case R.id.btn_clear : newGenre = EMPTY_GENRE; break;
        }
        if (isSelectingGenre1) {
            genre1 = newGenre;
            btnGenre1.setText(genresIds.get(newGenre));
        } else {
            genre2 = newGenre;
            btnGenre2.setText(genresIds.get(newGenre));
        }
        btnCancelClicked();
    }


    private void tryToShowAnimeList() {
        GAnalytics.reportAnimeListVisited(GAnalytics.ACTION_CLICKED);
        if ( !appSettings.readUserNickname().isEmpty() ) {
            GAnalytics.reportAnimeListVisited(GAnalytics.ACTION_VISITED);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                    ShikimoriService.WEBSERVICE_BASE_URL +
                            "/" +
                            appSettings.readUserNickname() +
                            "/list/anime"
            )));
        } else {
            UIUtils.showCustomToast(getActivity(), R.layout.toast_no_saved_titles_yet, Toast.LENGTH_SHORT);
        }
    }


    private void connectIdsToNames() {

        genresIds.put(EMPTY_GENRE, strSelect);

        genresIds.put(GENRE_ACTION, strGAction);
        genresIds.put(GENRE_ADVENTURE, strGAdventure);
        genresIds.put(GENRE_CARS, strGCars);
        genresIds.put(GENRE_COMEDY, strGComedy);
        genresIds.put(GENRE_DEMENTIA, strGDementia);

        genresIds.put(GENRE_DEMONS, strGDemons);
        genresIds.put(GENRE_MYSTERY, strGMystery);
        genresIds.put(GENRE_DRAMA, strGDrama);
        genresIds.put(GENRE_ECCHI, strGEcchi);
        genresIds.put(GENRE_FANTASY, strGFantasy);

        genresIds.put(GENRE_GAME, strGGame);
        genresIds.put(GENRE_HAREM, strGHarem);
        genresIds.put(GENRE_HISTORICAL, strGHistorical);
        genresIds.put(GENRE_HORROR, strGHorror);
        genresIds.put(GENRE_JOSEI, strGJosei);

        genresIds.put(GENRE_KIDS, strGKids);
        genresIds.put(GENRE_MAGIC, strGMagic);
        genresIds.put(GENRE_MARTIAL_ARTS, strGMatrialArts);
        genresIds.put(GENRE_MECHA, strGMecha);
        genresIds.put(GENRE_MILITARY, strGMilitary);

        genresIds.put(GENRE_MUSIC, strGMusic);
        genresIds.put(GENRE_PARODY, strGParody);
        genresIds.put(GENRE_POLICE, strGPolice);
        genresIds.put(GENRE_SAMURAI, strGSamurai);
        genresIds.put(GENRE_ROMANCE, strGRomance);

        genresIds.put(GENRE_SCHOOL, strGSchool);
        genresIds.put(GENRE_SCI_FI, strGSciFi);
        genresIds.put(GENRE_SHOUJO, strGShoujo);
        genresIds.put(GENRE_SHOUJO_AI, strGShoujoAi);
        genresIds.put(GENRE_SHOUNEN, strGShounen);

        genresIds.put(GENRE_SHOUNEN_AI, strGShounenAi);
        genresIds.put(GENRE_SPACE, strGSpace);
        genresIds.put(GENRE_SPORTS, strGSports);
        genresIds.put(GENRE_SUPER_POWER, strGSuperPower);
        genresIds.put(GENRE_VAMPIRES, strGVampire);

        genresIds.put(GENRE_SLICE_OF_LIFE, strGSliceOfLife);
        genresIds.put(GENRE_SUPERNATURAL, strGSuperNatural);
        genresIds.put(GENRE_PSYCHOLOGICAL, strGPsychological);
        genresIds.put(GENRE_THRILLER, strGThriller);
        genresIds.put(GENRE_SEINEN, strGSeinen);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View holder = inflater.inflate(R.layout.fragment_tabholder, container, false);
        unbinder = ButterKnife.bind(this, holder);
        return holder;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appSettings = activitySearch.getApp().getAppSettings();
        connectIdsToNames();

        TabHost mTabHost = (TabHost) view.findViewById(R.id.tabHost);
        mTabHost.setup();
        TabHost.TabSpec mSpec;

        mSpec = mTabHost.newTabSpec(strHeaderSearch);
        mSpec.setContent(R.id.tab_search);
        mSpec.setIndicator(strHeaderSearch);
        mTabHost.addTab(mSpec);

        mSpec = mTabHost.newTabSpec(strHeaderMenu);
        mSpec.setContent(R.id.tab_menu);
        mSpec.setIndicator(strHeaderMenu);
        mTabHost.addTab(mSpec);

        btnGenre1.setText(genresIds.get(genre1));
        btnGenre2.setText(genresIds.get(genre2));
    }


}
