package org.shikimori.c7j.rec.utils;

import android.content.Context;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.CoreActivity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Deque;
import java.util.LinkedList;


@SuppressWarnings("WeakerAccess")
public class BackStackUtils {

    public static final int SHORT_BACKSTACK_SIZE = 3;
    public static final int NORMAL_BACKSTACK_SIZE = 32;

    private final String BACKSTACK_FILENAME = "backstack-z";

    private Deque<FragmentInfo> backStack = new LinkedList<>();

    public Deque<FragmentInfo> getBackStack() {
        return backStack;
    }

    public int getSize() { return backStack.size(); }


    public boolean onBackPressedEmpty(ActivitySearch activitySearch) {
        if ( !backStack.isEmpty() && backStack.size() != 1 ) {
            backStack.removeFirst();
            FragmentInfo fragmentInfo = backStack.getFirst();
            int fragmentType = fragmentInfo.getFragmentType();
            switch (fragmentType) {

                case FragmentInfo.SEARCH_RESULTS:
                    activitySearch.loadSearchResultsFragment(fragmentInfo);
                    break;

                case FragmentInfo.ANIME_INFO:
                    activitySearch.loadAnimeInfoFragment(fragmentInfo);
                    break;

                case FragmentInfo.STARTING:
                    activitySearch.loadStartingFragment(fragmentInfo);
                    break;

                case FragmentInfo.AUTH:
                    activitySearch.loadAuthFragment(fragmentInfo);
                    break;

                case FragmentInfo.EMPTY:
                    activitySearch.loadEmptyFragment(fragmentInfo);
                    break;

                case FragmentInfo.TABHOLDER:
                    activitySearch.loadTabHolderFragment(fragmentInfo);
                    break;

                default:
                    onBackPressedEmpty(activitySearch);
            }
            return false;
        } else {
            return true;
        }
    }


    public void read(Context context) {
        FileInputStream fis = null;
        try {
            fis = context.openFileInput(BACKSTACK_FILENAME);
        } catch (FileNotFoundException e) { e.printStackTrace();}

        try {
            ObjectInputStream ois = new ObjectInputStream(fis);
            Deque<FragmentInfo> list = (Deque<FragmentInfo>) ois.readObject();
            backStack = list;
        } catch (IOException | ClassNotFoundException e) {
            L.t("failed to read backstack"); e.printStackTrace();
        } finally {
            try { if (fis != null) fis.close(); } catch (IOException e) { }
        }
    }


    /**
     * @param isShortBackStack Defines the number of backstack entities to write in memory.
     *                         When true - save 3 entities (recommended for low-end devices)
     *                         When false - save 32 entities (recommended for other devices)
     *                         Is set by user at preferencesFragment as AppSettings.KEY_IS_RESTORE_BACKSTACK_SHORT
     */
    public void write(CoreActivity context, boolean isShortBackStack) {

        int maxSizeOfBackStackToSave = isShortBackStack ? SHORT_BACKSTACK_SIZE : NORMAL_BACKSTACK_SIZE;
        FileOutputStream fos = null;
        if (!context.getAppSettings().readRestoreLastSession()) return;
        try {
            fos = context.openFileOutput(BACKSTACK_FILENAME, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) { e.printStackTrace(); }

        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            int size = ( backStack.size() > maxSizeOfBackStackToSave) ?
                    maxSizeOfBackStackToSave : backStack.size();
            Deque<FragmentInfo> backStackTemp =
                    new LinkedList<>(new LinkedList<>(backStack).subList(0, size)); //indexoutofboundexception care!
            oos.writeObject(backStackTemp);
        } catch (IOException e) {
            L.t("failed to write backstack: " + e.toString()); e.printStackTrace();
        } finally {
            try { if (fos != null) fos.close(); } catch (IOException e) { }
        }
    }

}
