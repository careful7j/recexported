package org.shikimori.c7j.rec.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class AccessToken implements Serializable {

    @SerializedName("api_access_token")
    private String ApiAccessToken;

    public String getApiAccessToken() {
        return ApiAccessToken;
    }

    public void setApiAccessToken(String apiAccessToken) {
        ApiAccessToken = apiAccessToken;
    }

}
