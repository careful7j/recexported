package org.shikimori.c7j.rec.data.animeinfo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
class Screenshot implements Serializable {

    private String original;
    private String preview;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public String getOriginal() {
        return original;
    }

    
    public void setOriginal(String original) {
        this.original = original;
    }

    
    public String getPreview() {
        return preview;
    }

    
    public void setPreview(String preview) {
        this.preview = preview;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
