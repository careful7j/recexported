
package org.shikimori.c7j.rec.data.whoami;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class WhoAmI implements Serializable {

    private Integer id;
    private String nickname;
    private String avatar;
    @SerializedName("image")
    private UserImage userImage;
    @SerializedName("last_online_at")
    private String lastOnlineAt;
    private String name;
    private String sex;
    private String website;
    @SerializedName("birth_on")
    private String birthOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public UserImage getImage() {
        return userImage;
    }

    public void setImage(UserImage image) {
        this.userImage = image;
    }

    public String getLastOnlineAt() {
        return lastOnlineAt;
    }

    public void setLastOnlineAt(String lastOnlineAt) {
        this.lastOnlineAt = lastOnlineAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBirthOn() {
        return birthOn;
    }

    public void setBirthOn(String birthOn) {
        this.birthOn = birthOn;
    }

}
