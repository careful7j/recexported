package org.shikimori.c7j.rec.network;

import org.shikimori.c7j.rec.utils.L;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;


/**
 * Executes API calls (which don't require authorization) to shikimori webservice
 * @see ShikimoriAuthorizedService
 */
public class ShikimoriService {

    public static final String WEBSERVICE_BASE_URL = "https://shikimori.org";
    static final int CONNECTION_TIMEOUT = 7000;


    private ShikimoriService() { /* Do not inherit */ };


    public static ShikimoriApi createShikimoriService(final String shikimoriAppId) {
        
        Retrofit.Builder builder = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(WEBSERVICE_BASE_URL);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new Interceptor() {

                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Request chainReq = request.newBuilder()
                                .addHeader("Content-type", "application/json")
                                .addHeader("User-Agent", shikimoriAppId)
                                .build();

                        L.n("Connecting to: " + chainReq.url());
                        L.n("" + chainReq.headers().toString());
                        return chain.proceed(chainReq);
                    }

                }).build();

        builder.client(client);
        return builder.build().create(ShikimoriApi.class);
    }
}
