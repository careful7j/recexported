package org.shikimori.c7j.rec.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/** This is the fragment with wallpapers which appears after app launched */
public class StartingFragment extends Fragment {

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.STARTING);
    public FragmentInfo getFragmentInfo() { return fragmentInfo; }

    private Unbinder unbinder;


    @OnClick(R.id.tv_artist_clickable) void btnArtistVkClicked() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SettingsFragment.ARTIST_PAGE_URL)));
        GAnalytics.reportAuthorFromStartingFragmentVisited(GAnalytics.ACTION_FROM_STARTING_FRAGMENT);
    }

    @OnClick(R.id.btn_round_settings) void btnSettingsClicked() {
        ((ActivitySearch) getActivity()).loadPrefsFragment(null);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if ( unbinder != null ) unbinder.unbind();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View holder = inflater.inflate(R.layout.fragment_starting, container, false);
        unbinder = ButterKnife.bind(this, holder);
        return holder;
    }
}
