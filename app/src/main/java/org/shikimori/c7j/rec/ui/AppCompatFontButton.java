package org.shikimori.c7j.rec.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import org.shikimori.c7j.rec.MyApp;


/** Button with custom font used by default */
public class AppCompatFontButton extends AppCompatButton {


    public AppCompatFontButton(Context context) {
        super( context );
        setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL );
    }


    public AppCompatFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL);
    }


    public AppCompatFontButton(Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL );
    }

}
