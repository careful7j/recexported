package org.shikimori.c7j.rec.data.animeinfo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Genre implements Serializable {

    private Integer id;
    private String name;
    private String russian;
    private String kind;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public String getRussian() {
        return russian;
    }

    
    public void setRussian(String russian) {
        this.russian = russian;
    }

    
    public String getKind() {
        return kind;
    }

    
    public void setKind(String kind) {
        this.kind = kind;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
