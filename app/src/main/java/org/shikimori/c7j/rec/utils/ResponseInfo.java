package org.shikimori.c7j.rec.utils;

import java.io.Serializable;


public class ResponseInfo implements Serializable {

    public static final int UNDEFINED = 0;
    public static final int GET_SEARCH_LIST = 1;
    public static final int GET_ANIMEINFO = 2;
    public static final int GET_CHARACTERS_LIST = 3;
    public static final int GET_RELATED_LIST = 4;
    public static final int GET_SCREENSHOTS = 5;

    private int responseType;
    private Object apiResponse;


    public ResponseInfo(int responseType, Object apiResponse) {
        this.responseType = responseType;
        this.apiResponse = apiResponse;
    }


    public int getResponseType() {
        return responseType;
    }

    public void setResponseType(int responseType) {
        this.responseType = responseType;
    }

    public Object getApiResponse() { return apiResponse; }

    public void setApiResponse(Object apiResponse) {
        this.apiResponse = apiResponse;
    }

}
