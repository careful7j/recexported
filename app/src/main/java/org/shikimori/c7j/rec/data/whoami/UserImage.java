package org.shikimori.c7j.rec.data.whoami;

import java.io.Serializable;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class UserImage implements Serializable {

    private String x160;
    private String x148;
    private String x80;
    private String x64;
    private String x48;
    private String x32;
    private String x16;

    public String getX160() {
        return x160;
    }

    public void setX160(String x160) {
        this.x160 = x160;
    }

    public String getX148() {
        return x148;
    }

    public void setX148(String x148) {
        this.x148 = x148;
    }

    public String getX80() {
        return x80;
    }

    public void setX80(String x80) {
        this.x80 = x80;
    }

    public String getX64() {
        return x64;
    }

    public void setX64(String x64) {
        this.x64 = x64;
    }

    public String getX48() {
        return x48;
    }

    public void setX48(String x48) {
        this.x48 = x48;
    }

    public String getX32() {
        return x32;
    }

    public void setX32(String x32) {
        this.x32 = x32;
    }

    public String getX16() {
        return x16;
    }

    public void setX16(String x16) {
        this.x16 = x16;
    }

}
