package org.shikimori.c7j.rec.ui;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.CoreActivity;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.data.Related;
import org.shikimori.c7j.rec.fragments.AnimeInfoFragment;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.utils.L;

import java.util.List;
import java.util.Locale;

import static org.shikimori.c7j.rec.ActivitySearch.lastSearchQuery;


public class AdapterRelated extends RecyclerView.Adapter<AdapterRelated.ViewHolder> {

    private List<Related> dataSet;
    private AnimeInfoFragment animeInfoFragment = null;

    private String strSideStory;
    private String strSummary;
    private String strOther;
    private String strSequel;
    private String strPrequel;
    private String strSpinOff;
    private String strFullStory;
    private String strAlternativeSetting;
    private String strAlternativeVersion;
    private String strCharacter;
    private String strParentStory;

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvRelated;
        TextView tvRelatedRelation;
        LinearLayout relatedItem;


        ViewHolder(View v) {
            super(v);
            tvRelated = (TextView) v.findViewById(R.id.tv_related_name);
            tvRelatedRelation = (TextView) v.findViewById(R.id.tv_related_relation);
            relatedItem = (LinearLayout) v.findViewById(R.id.related_item);
        }
    }


    public AdapterRelated(List<Related> dataSet, AnimeInfoFragment animeInfoFragment) {
        this.dataSet = dataSet;
        this.animeInfoFragment = animeInfoFragment;
    }


    public void updateDataSet( List<Related> dataSet ) {
        this.dataSet = dataSet;
        this.notifyDataSetChanged();
    }


    @Override
    public AdapterRelated.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_related, parent, false);
        Resources res = v.getContext().getResources();
        strSideStory = res.getString(R.string.frag_animeinfo_related_sidestory);
        strSummary = res.getString(R.string.frag_animeinfo_related_summary);
        strOther = res.getString(R.string.frag_animeinfo_related_other);
        strSequel = res.getString(R.string.frag_animeinfo_related_sequel);
        strPrequel = res.getString(R.string.frag_animeinfo_related_prequel);
        strSpinOff = res.getString(R.string.frag_animeinfo_related_spinoff);
        strFullStory = res.getString(R.string.frag_animeinfo_related_fullstory);
        strAlternativeSetting = res.getString(R.string.frag_animeinfo_related_alternativesetting);
        strAlternativeVersion = res.getString(R.string.frag_animeinfo_related_alternativevrsion);
        strCharacter = res.getString(R.string.frag_animeinfo_related_character);
        strParentStory = res.getString(R.string.frag_animeinfo_related_parent_story);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final AdapterRelated.ViewHolder holder, int position) {
        final Related related = dataSet.get(position);
        String relatedName = related.getRelation();
        String relatedNameRus = "";
        String animeName = related.getAnime().getName();
        String animeNameRus = related.getAnime().getRussian();

        switch(relatedName) {
            case "Side story" : relatedNameRus = strSideStory; break;
            case "Summary" : relatedNameRus = strSummary; break;
            case "Other" : relatedNameRus = strOther; break;
            case "Sequel" : relatedNameRus = strSequel; break;
            case "Prequel" : relatedNameRus = strPrequel; break;
            case "Spin-off" : relatedNameRus = strSpinOff; break;
            case "Full story" : relatedNameRus = strFullStory; break;
            case "Alternative setting" : relatedNameRus = strAlternativeSetting; break;
            case "Character" : relatedNameRus = strCharacter; break;
            case "Alternative version" : relatedNameRus = strAlternativeVersion; break;
            case "Parent story" : relatedNameRus = strParentStory; break;
            default : relatedNameRus = relatedName;
        }

        boolean isSetLanguageToRussian =
                ( lastSearchQuery.length() > 0 &&
                lastSearchQuery.charAt(0) >= 'А' &&
                lastSearchQuery.charAt(0) <= 'я' ) || lastSearchQuery.isEmpty();

        if (!CoreActivity.locale.equals(CoreActivity.LOCALE_RUSSIAN)) {
            holder.tvRelated.setText(animeName);
            holder.tvRelatedRelation.setText(relatedName );
        } else if (isSetLanguageToRussian) {
            holder.tvRelated.setText(animeNameRus);
            holder.tvRelatedRelation.setText(relatedNameRus);
        } else {
            holder.tvRelated.setText(animeName);
            holder.tvRelatedRelation.setText(relatedNameRus);
        }

        holder.relatedItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ActivitySearch activitySearch = animeInfoFragment.getActivitySearch();
                String relatedId = "" + related.getAnime().getId();
                activitySearch.getRm().requestAnimeInfo( activitySearch, relatedId, false );
                activitySearch.getRm().requestRoles( activitySearch, relatedId );
                activitySearch.getRm().requestRelated( activitySearch, relatedId );
                activitySearch.getRm().requestScreenshots( activitySearch, relatedId );
                GAnalytics.reportRelatedVisited();
            }
        });
    }


    @Override
    public int getItemCount() {
        if (dataSet == null) {
            return 0;
        } else return dataSet.size();
    }


    public void onDestroy() {
        animeInfoFragment = null;
    }
}
