package org.shikimori.c7j.rec.data.animeinfo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Studio implements Serializable {

    private Integer id;
    private String name;
    private String filteredName;
    private Boolean real;
    private String image;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public String getFilteredName() {
        return filteredName;
    }

    
    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    
    public Boolean getReal() {
        return real;
    }

    
    public void setReal(Boolean real) {
        this.real = real;
    }

    
    public String getImage() {
        return image;
    }

    
    public void setImage(String image) {
        this.image = image;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
