
package org.shikimori.c7j.rec.data;


import com.google.gson.annotations.SerializedName;

import org.shikimori.c7j.rec.data.animerates.Anime;

import java.io.Serializable;


@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Related implements Serializable {

    private String relation;
    @SerializedName("relation_russian")
    private String relationRussian;
    private Anime anime;
    private Object manga;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getRelationRussian() {
        return relationRussian;
    }

    public void setRelationRussian(String relationRussian) {
        this.relationRussian = relationRussian;
    }

    public Anime getAnime() {
        return anime;
    }

    public void setAnime(Anime anime) {
        this.anime = anime;
    }

    public Object getManga() {
        return manga;
    }

    public void setManga(Object manga) {
        this.manga = manga;
    }

}
