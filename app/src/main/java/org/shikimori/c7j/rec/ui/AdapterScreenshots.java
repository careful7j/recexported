package org.shikimori.c7j.rec.ui;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.shikimori.c7j.rec.AppSettings;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.data.Screenshot;
import org.shikimori.c7j.rec.network.ShikimoriService;

import java.util.ArrayList;
import java.util.List;


public class AdapterScreenshots extends RecyclerView.Adapter<AdapterScreenshots.ViewHolder> {

    private List<Screenshot> dataSet;

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgScreenshot;
        CardView cardView;

        ViewHolder(View v) {
            super(v);
            imgScreenshot = (ImageView) v.findViewById(R.id.img_screenshot);
            cardView = (CardView) v.findViewById(R.id.cardview_screenshot);
        }
    }

    public AdapterScreenshots(ArrayList<Screenshot> dataSet) {
        this.dataSet = dataSet;
    }


    public void updateDataSet( List<Screenshot> dataSet, AppSettings appSettings, Context context ) {

        final int SCREENSHOTS_PER_SCREEN_LIMIT = 16;
        final int SCREENSHOTS_PER_SCREEN_EXTENDED_LIMIT = 64;

        if (    appSettings != null &&
                appSettings.readIsScreenshotsLimitSet() &&
                dataSet.size() > SCREENSHOTS_PER_SCREEN_LIMIT)
        {
            this.dataSet = dataSet.subList(0, SCREENSHOTS_PER_SCREEN_LIMIT);
        } else if ( dataSet.size() > SCREENSHOTS_PER_SCREEN_EXTENDED_LIMIT) {
            this.dataSet = dataSet.subList(0, SCREENSHOTS_PER_SCREEN_EXTENDED_LIMIT);
        } else { this.dataSet = dataSet; }

        NetworkInfo info = (((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo());
        boolean isMobileNetwork =
                (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);

        if (    appSettings == null ||
                ( appSettings.readIsDontLoadScreenshotsOver3G() && isMobileNetwork ) )
        {
            this.dataSet = new ArrayList<>();
        }
        this.notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(final AdapterScreenshots.ViewHolder holder, int position) {

        Screenshot screenshot = dataSet.get(position);
        if ( screenshot != null ) {
            Context context = holder.imgScreenshot.getRootView().getContext();
            Glide.with(context)
                    .load(ShikimoriService.WEBSERVICE_BASE_URL + screenshot.getOriginal())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.cardView.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .centerCrop()
                    .into(holder.imgScreenshot);
        }
    }


    @Override
    public AdapterScreenshots.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_screenshot, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        if (dataSet == null) {
            return 0;
        } else return dataSet.size();
    }
}
