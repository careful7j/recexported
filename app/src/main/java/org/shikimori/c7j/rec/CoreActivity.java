package org.shikimori.c7j.rec;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.shikimori.c7j.rec.network.RequestManager;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.ui.UIUtils;
import org.shikimori.c7j.rec.utils.BackStackUtils;
import org.shikimori.c7j.rec.utils.L;
import org.shikimori.c7j.rec.utils.RxUtils;

import java.util.Locale;

import rx.subscriptions.CompositeSubscription;


public class CoreActivity extends AppCompatActivity {

    public static final String LOCALE_RUSSIAN = "ru";

    protected CompositeSubscription _subscriptions = new CompositeSubscription();

    protected MyApp app = null;
    protected AppSettings appSettings;
    protected BackStackUtils backStackUtils;
    protected RequestManager requestManager;

    public MyApp getApp() { return app; }
    public AppSettings getAppSettings() { return appSettings; }
    public RequestManager getRm() {
        return requestManager;
    }

    public CompositeSubscription get_subscriptions() {
        return _subscriptions;
    }

    public static String locale = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = ((MyApp) getApplication());
        appSettings = app.getAppSettings();
        backStackUtils = app.getBackStackUtils();
        requestManager = new RequestManager(getString(R.string.shikimori_app_id));
        checkIfEnforceRussianLanguage();
    }


    @Override
    protected void onResume() {
        super.onResume();
        _subscriptions = RxUtils.getNewCompositeSubIfUnsubscribed(_subscriptions);
        GAnalytics.searchRequestsCounter = appSettings.readSearchApiCallsCounter();
    }


    @Override
    protected void onPause() {
        super.onPause();
        RxUtils.unsubscribeIfNotNull(_subscriptions);
        backStackUtils.write(this, appSettings.readIsRestoreBackStackShort());
        appSettings.writeSearchApiCallsCounter(GAnalytics.searchRequestsCounter);
    }


    protected void logMemory() {
        System.gc();
        final Runtime runtime = Runtime.getRuntime();
        final long usedMemoryKb = (runtime.totalMemory() - runtime.freeMemory()) / 1024L;
        final long fullMemoryKb = runtime.maxMemory() / 1024L;
        L.t("Memory: " + usedMemoryKb + "/" + fullMemoryKb);
    }


    /** Checks if network is available */
    public boolean isInternetConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if ( networkInfo != null && networkInfo.isConnected() )
            return true;
        else {
            UIUtils.showCustomToast(this, R.layout.toast_nointernet, Toast.LENGTH_LONG);
            return false;
        }
    }


    /** Checks if network is available without toast */
    protected boolean isInternetConnectedCheckSilently() {
        ConnectivityManager connMgr =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    private void checkIfEnforceRussianLanguage() {
        locale = Locale.getDefault().getLanguage();
        if (getAppSettings().readAlwaysUseRussian()) {
            Configuration config = new Configuration( this.getResources().getConfiguration() );
            config.locale = Locale.getDefault();
            config.locale = new Locale( "ru", "RU" );
            this.getResources().updateConfiguration( config, this.getResources().getDisplayMetrics() );
            locale = LOCALE_RUSSIAN;
        }
    }

}
