package org.shikimori.c7j.rec.data.animeinfo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class RatesScoresStat implements Serializable {

    private Integer name;
    private Integer value;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public Integer getName() {
        return name;
    }

    
    public void setName(Integer name) {
        this.name = name;
    }

    
    public Integer getValue() {
        return value;
    }

    
    public void setValue(Integer value) {
        this.value = value;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
