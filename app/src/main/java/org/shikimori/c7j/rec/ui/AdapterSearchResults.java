package org.shikimori.c7j.rec.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.data.search.Image;
import org.shikimori.c7j.rec.data.search.SearchResult;
import org.shikimori.c7j.rec.network.ShikimoriService;

import java.util.List;


@SuppressWarnings("WeakerAccess")
public class AdapterSearchResults extends BaseAdapter {

    private List<SearchResult> dataSet;


    public AdapterSearchResults(List<SearchResult> dataSet) {
        this.dataSet = dataSet;
    }


    public static class ViewHolder {
        public TextView tvTitle;
        public TextView tvType;
        public TextView tvYear;
        public ImageView imgAnimePoster;
    }


    @Override public Object getItem(int position) { return null; }

    @Override public long getItemId(int position) { return 0; }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder grid;
        Context context = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null ) {
            grid = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_anime, null);
            grid.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            grid.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            grid.tvYear = (TextView) convertView.findViewById(R.id.tv_year);
            grid.imgAnimePoster = (ImageView) convertView.findViewById(R.id.img_anime_poster);
            convertView.setTag(grid);
        } else {
            grid = (ViewHolder) convertView.getTag();
        }

        SearchResult searchResult = dataSet.get(position);
        if ( searchResult != null ) {

            String lastSearchQuery = ActivitySearch.lastSearchQuery;
            boolean setTitlesLanguageToRussian =
                    lastSearchQuery.length() > 0 &&
                    lastSearchQuery.charAt(0) >= 'А' &&
                    lastSearchQuery.charAt(0) <= 'я' &&
                    searchResult.getRussian() != null;

            if ( setTitlesLanguageToRussian ) {
                grid.tvTitle.setText(searchResult.getRussian());
            } else {
                if (searchResult.getName() != null) {
                    grid.tvTitle.setText(searchResult.getName());
                } else grid.tvTitle.setText("");
            }

            String type = searchResult.getKind();
            if ( type != null ) {
                grid.tvType.setText(type.toUpperCase());
            }

            String airedOn = searchResult.getAiredOn();
            if ( airedOn != null && airedOn.length() >= 4 ) {
                grid.tvYear.setText( airedOn.substring( 0, 4 ) );
            }

            Image image = searchResult.getImage();
            if ( image != null ) {
                Glide.with(context)
                        .load(ShikimoriService.WEBSERVICE_BASE_URL + image.getPreview())
                        .centerCrop()
                        .into(grid.imgAnimePoster);
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        if (dataSet == null) {
            return 0;
        } else return dataSet.size();
    }
}
