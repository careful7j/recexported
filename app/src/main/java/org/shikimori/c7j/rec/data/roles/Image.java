
package org.shikimori.c7j.rec.data.roles;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Image implements Serializable {

    private String original;
    private String preview;
    private String x96;
    private String x48;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public String getOriginal() {
        return original;
    }

    
    public void setOriginal(String original) {
        this.original = original;
    }

    
    public String getPreview() {
        return preview;
    }

    
    public void setPreview(String preview) {
        this.preview = preview;
    }

    
    public String getX96() {
        return x96;
    }

    
    public void setX96(String x96) {
        this.x96 = x96;
    }

    
    public String getX48() {
        return x48;
    }

    
    public void setX48(String x48) {
        this.x48 = x48;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Image{" +
                "original='" + original + '\'' +
                ", preview='" + preview + '\'' +
                ", x96='" + x96 + '\'' +
                ", x48='" + x48 + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
