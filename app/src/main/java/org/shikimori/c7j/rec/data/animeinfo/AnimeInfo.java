package org.shikimori.c7j.rec.data.animeinfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class AnimeInfo implements Serializable {

    private Integer id;
    private String name;
    private String russian;
    private Image image;
    private String url;
    private String kind;
    private String status;
    private Integer episodes;
    @SerializedName("episodes_aired")
    private Integer episodesAired;
    @SerializedName("aired_on")
    private String airedOn;
    @SerializedName("released_on")
    private String releasedOn;
    private String rating;
    private List<String> english = new ArrayList<String>();
    private List<String> japanese = new ArrayList<String>();
    private List<String> synonyms = new ArrayList<String>();
    private Integer duration;
    private String score;
    private String description;
    @SerializedName("description_html")
    private String descriptionHtml;
    @SerializedName("description_source")
    private String descriptionSource;
    private Boolean favoured;
    private Boolean anons;
    private Boolean ongoing;
    @SerializedName("thread_id")
    private Integer threadId;
    @SerializedName("topic_id")
    private Integer topicId;
    @SerializedName("world_art_id")
    private Integer worldArtId;
    @SerializedName("myanimelist_id")
    private Integer myanimelistId;
    @SerializedName("ani_db_id")
    private Integer aniDbId;
    @SerializedName("rates_scores_stats")
    private List<RatesScoresStat> ratesScoresStats = new ArrayList<RatesScoresStat>();
    @SerializedName("rates_statuses_stats")
    private List<RatesStatusesStat> ratesStatusesStats = new ArrayList<RatesStatusesStat>();
    @SerializedName("updated_at")
    private String updatedAt;
    private List<Genre> genres = new ArrayList<Genre>();
    private List<Studio> studios = new ArrayList<Studio>();
    private List<Video> videos = new ArrayList<Video>();
    private List<Screenshot> screenshots = new ArrayList<Screenshot>();
    @SerializedName("user_rate")
    private Object userRate;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public String getRussian() {
        return russian;
    }


    public void setRussian(String russian) {
        this.russian = russian;
    }

    
    public Image getImage() {
        return image;
    }

    
    public void setImage(Image image) {
        this.image = image;
    }

    
    public String getUrl() {
        return url;
    }

    
    public void setUrl(String url) {
        this.url = url;
    }

    
    public String getKind() {
        return kind;
    }

    
    public void setKind(String kind) {
        this.kind = kind;
    }

    
    public String getStatus() {
        return status;
    }

    
    public void setStatus(String status) {
        this.status = status;
    }

    
    public Integer getEpisodes() {
        return episodes;
    }

    
    public void setEpisodes(Integer episodes) {
        this.episodes = episodes;
    }

    
    public Integer getEpisodesAired() {
        return episodesAired;
    }

    
    public void setEpisodesAired(Integer episodesAired) {
        this.episodesAired = episodesAired;
    }

    
    public String getAiredOn() {
        return airedOn;
    }

    
    public void setAiredOn(String airedOn) {
        this.airedOn = airedOn;
    }

    
    public String getReleasedOn() {
        return releasedOn;
    }

    
    public void setReleasedOn(String releasedOn) {
        this.releasedOn = releasedOn;
    }

    
    public String getRating() {
        return rating;
    }

    
    public void setRating(String rating) {
        this.rating = rating;
    }

    
    public List<String> getEnglish() {
        return english;
    }

    
    public void setEnglish(List<String> english) {
        this.english = english;
    }

    
    public List<String> getJapanese() {
        return japanese;
    }

    
    public void setJapanese(List<String> japanese) {
        this.japanese = japanese;
    }

    
    public List<String> getSynonyms() {
        return synonyms;
    }

    
    public void setSynonyms(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    
    public Integer getDuration() {
        return duration;
    }

    
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    
    public String getScore() {
        return score;
    }

    
    public void setScore(String score) {
        this.score = score;
    }

    
    public String getDescription() {
        return description;
    }

    
    public void setDescription(String description) {
        this.description = description;
    }

    
    public String getDescriptionHtml() {
        return descriptionHtml;
    }

    
    public void setDescriptionHtml(String descriptionHtml) {
        this.descriptionHtml = descriptionHtml;
    }

    
    public String getDescriptionSource() {
        return descriptionSource;
    }

    
    public void setDescriptionSource(String descriptionSource) {
        this.descriptionSource = descriptionSource;
    }

    
    public Boolean getFavoured() {
        return favoured;
    }

    
    public void setFavoured(Boolean favoured) {
        this.favoured = favoured;
    }

    
    public Boolean getAnons() {
        return anons;
    }

    
    public void setAnons(Boolean anons) {
        this.anons = anons;
    }

    
    public Boolean getOngoing() {
        return ongoing;
    }

    
    public void setOngoing(Boolean ongoing) {
        this.ongoing = ongoing;
    }

    
    public Integer getThreadId() {
        return threadId;
    }

    
    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    
    public Integer getTopicId() {
        return topicId;
    }

    
    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    
    public Integer getWorldArtId() {
        return worldArtId;
    }

    
    public void setWorldArtId(Integer worldArtId) {
        this.worldArtId = worldArtId;
    }

    
    public Integer getMyanimelistId() {
        return myanimelistId;
    }

    
    public void setMyanimelistId(Integer myanimelistId) {
        this.myanimelistId = myanimelistId;
    }

    
    public Integer getAniDbId() {
        return aniDbId;
    }

    
    public void setAniDbId(Integer aniDbId) {
        this.aniDbId = aniDbId;
    }

    
    public List<RatesScoresStat> getRatesScoresStats() {
        return ratesScoresStats;
    }

    
    public void setRatesScoresStats(List<RatesScoresStat> ratesScoresStats) {
        this.ratesScoresStats = ratesScoresStats;
    }

    
    public List<RatesStatusesStat> getRatesStatusesStats() {
        return ratesStatusesStats;
    }

    
    public void setRatesStatusesStats(List<RatesStatusesStat> ratesStatusesStats) {
        this.ratesStatusesStats = ratesStatusesStats;
    }

    
    public String getUpdatedAt() {
        return updatedAt;
    }

    
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    
    public List<Genre> getGenres() {
        return genres;
    }

    
    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    
    public List<Studio> getStudios() {
        return studios;
    }

    
    public void setStudios(List<Studio> studios) {
        this.studios = studios;
    }

    
    public List<Video> getVideos() {
        return videos;
    }

    
    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    
    public List<Screenshot> getScreenshots() {
        return screenshots;
    }

    
    public void setScreenshots(List<Screenshot> screenshots) {
        this.screenshots = screenshots;
    }

    
    public Object getUserRate() {
        return userRate;
    }

    
    public void setUserRate(Object userRate) {
        this.userRate = userRate;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "AnimeInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", russian='" + russian + '\'' +
                ", image=" + image +
                ", url='" + url + '\'' +
                ", kind='" + kind + '\'' +
                ", status='" + status + '\'' +
                ", episodes=" + episodes +
                ", episodesAired=" + episodesAired +
                ", airedOn='" + airedOn + '\'' +
                ", releasedOn='" + releasedOn + '\'' +
                ", rating='" + rating + '\'' +
                ", english=" + english +
                ", japanese=" + japanese +
                ", synonyms=" + synonyms +
                ", duration=" + duration +
                ", score='" + score + '\'' +
                ", description='" + description + '\'' +
                ", descriptionHtml='" + descriptionHtml + '\'' +
                ", descriptionSource='" + descriptionSource + '\'' +
                ", favoured=" + favoured +
                ", anons=" + anons +
                ", ongoing=" + ongoing +
                ", threadId=" + threadId +
                ", topicId=" + topicId +
                ", worldArtId=" + worldArtId +
                ", myanimelistId=" + myanimelistId +
                ", aniDbId=" + aniDbId +
                ", ratesScoresStats=" + ratesScoresStats +
                ", ratesStatusesStats=" + ratesStatusesStats +
                ", updatedAt='" + updatedAt + '\'' +
                ", genres=" + genres +
                ", studios=" + studios +
                ", videos=" + videos +
                ", screenshots=" + screenshots +
                ", userRate=" + userRate +
                ", additionalProperties=" + additionalProperties +
                "}\n";
    }
}
