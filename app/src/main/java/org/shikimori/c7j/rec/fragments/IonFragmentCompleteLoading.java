package org.shikimori.c7j.rec.fragments;

import org.shikimori.c7j.rec.utils.FragmentInfo;


public interface IonFragmentCompleteLoading {

    void onFragmentCompleteLoading(FragmentInfo tempfragmentInfo);

}
