package org.shikimori.c7j.rec.utils;

import android.util.Log;


public class L {

    private static boolean enabled = org.shikimori.c7j.rec.BuildConfig.BUILD_TYPE.equals("debug");

    public static void t(Object message) {
        if (!enabled) return;
        Log.e("#TEST", "" + message);
    }

    public static void n(Object message) {
        if (!enabled) return;
        Log.e("#NET", "" + message);
    }

    public static void d(Object message) {
        if (!enabled) return;
        Log.e("#DB", "" + message);
    }

    public static void e(Object message) {
        if (!enabled) return;
        Log.e("#ERROR", "" + message);
    }

    public static void t(String logHeader, Object message) {
        if (!enabled) return;
        Log.e("#" + logHeader, "" + message);
    }

}
