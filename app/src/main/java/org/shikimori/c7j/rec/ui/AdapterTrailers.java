package org.shikimori.c7j.rec.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.data.animeinfo.Video;
import org.shikimori.c7j.rec.services.GAnalytics;

import java.util.List;


public class AdapterTrailers extends RecyclerView.Adapter<AdapterTrailers.ViewHolder> {

    private List<Video> dataSet;
    private String emptyTrailerString;

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvVideoName;
        TextView tvVideoHosting;
        LinearLayout trailerItem;


        ViewHolder(View v) {
            super(v);
            tvVideoName = (TextView) v.findViewById(R.id.tv_video_name);
            tvVideoHosting = (TextView) v.findViewById(R.id.tv_video_hosting);
            trailerItem = (LinearLayout) v.findViewById(R.id.trailer_item);
        }
    }


    public AdapterTrailers(List<Video> dataSet, String emptyTrailerString) {
        this.dataSet = dataSet;
        this.emptyTrailerString = emptyTrailerString;
    }


    public void updateDataSet( List<Video> dataSet ) {
        this.dataSet = dataSet;
        this.notifyDataSetChanged();
    }


    @Override
    public AdapterTrailers.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trailer, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(AdapterTrailers.ViewHolder holder, int position) {
        final Video video = dataSet.get(position);
        final String hosting = video.getHosting();
        final String name = video.getName();
        final String url = video.getUrl();

        if (hosting != null && !hosting.isEmpty()) {
            holder.tvVideoHosting.setText(hosting.toUpperCase());
        }
        if (name != null && !name.isEmpty()) {
            holder.tvVideoName.setText(name);
        } else holder.tvVideoName.setText(emptyTrailerString);

        final Context context = holder.tvVideoName.getRootView().getContext();
        holder.trailerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                GAnalytics.reportTrailerWatched();
            }
        });
    }


    @Override
    public int getItemCount() {
        if (dataSet == null) {
            return 0;
        } else return dataSet.size();
    }
}
