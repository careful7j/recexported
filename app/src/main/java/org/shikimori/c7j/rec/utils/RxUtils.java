package org.shikimori.c7j.rec.utils;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public class RxUtils {


    public static void unsubscribeIfNotNull(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }


    public static CompositeSubscription getNewCompositeSubIfUnsubscribed(CompositeSubscription subscription) {
        if (subscription == null || subscription.isUnsubscribed()) {
            return new CompositeSubscription();
        } else return subscription;
    }

    /** Since normal printStackTrace() is broken when using Rx */
    public static void logOnErrorStackTrace(Throwable e) {
        StackTraceElement[] element = e.getStackTrace();
        for (StackTraceElement elem : element) {
            L.t("Got Error: " + elem.toString());
        }
    }
}
