package org.shikimori.c7j.rec.data.animeinfo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Video implements Serializable {

    private Integer id;
    private String url;
    private String imageUrl;
    private String playerUrl;
    private String name;
    private String kind;
    private String hosting;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getUrl() {
        return url;
    }

    
    public void setUrl(String url) {
        this.url = url;
    }

    
    public String getImageUrl() {
        return imageUrl;
    }

    
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    
    public String getPlayerUrl() {
        return playerUrl;
    }

    
    public void setPlayerUrl(String playerUrl) {
        this.playerUrl = playerUrl;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public String getKind() {
        return kind;
    }

    
    public void setKind(String kind) {
        this.kind = kind;
    }

    
    public String getHosting() {
        return hosting;
    }

    
    public void setHosting(String hosting) {
        this.hosting = hosting;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
