package org.shikimori.c7j.rec.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.shikimori.c7j.rec.AppSettings;
import org.shikimori.c7j.rec.ui.UIUtils;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.network.ShikimoriService;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingsFragment extends ABaseFragment {

    public static final String ARTIST_PAGE_URL = "https://vk.com/artsbymikuo";
    public static final String SHIKIMORI_CLUB_URL = "https://shikimori.org/clubs/903-klub-mobilnogo-prilozheniya-anime-rekomendatsii";
    public static final String SHIKIMORI_ANIME_NEWS = "https://shikimori.org/forum/news";

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.SETTINGS);
    public FragmentInfo getFragmentInfo() { return fragmentInfo; }

    private AppSettings appSettings = null;


    @OnClick(R.id.btn_advanced_settings) void btnAdvancedSettingsClicked() {
        activitySearch.loadPrefsFragment(null);
    }

    @OnClick(R.id.btn_anime_news) void btnAnimeNewsClicked() {
        GAnalytics.reportAnimeNewsVisited();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SHIKIMORI_ANIME_NEWS)));
    }

    @OnClick(R.id.btn_my_animelist) void btnMyAnimelistClicked() {
        tryToShowAnimeList();
    }

    @OnClick(R.id.btn_to_shikimori_club) void btnToShikimoriClubClicked() {
        GAnalytics.reportShikimoriClubVisited();
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(SHIKIMORI_CLUB_URL + "?utm_source=" + getString(R.string.shikimori_app_id))));
    }

    @OnClick(R.id.btn_rate_us) void btnRateUsClicked() {
        GAnalytics.reportRateUs();
        UIUtils.openAppGooglePlayMarketPage(activitySearch);
    }

    @OnClick(R.id.btn_artist_page) void btnArtistVkClicked() {
        GAnalytics.reportAuthorFromStartingFragmentVisited(GAnalytics.ACTION_FROM_SETTINGS_FRAGMENT);
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ARTIST_PAGE_URL)));
    }


    private void tryToShowAnimeList() {
        GAnalytics.reportAnimeListVisited(GAnalytics.ACTION_CLICKED);
        if ( !appSettings.readUserNickname().isEmpty() ) {
            GAnalytics.reportAnimeListVisited(GAnalytics.ACTION_VISITED);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                    ShikimoriService.WEBSERVICE_BASE_URL +
                            "/" +
                            appSettings.readUserNickname() +
                            "/list/anime" +
                            "?utm_source=" +
                            getString(R.string.shikimori_app_id)
            )));
        } else {
            UIUtils.showCustomToast(getActivity(), R.layout.toast_no_saved_titles_yet, Toast.LENGTH_SHORT);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View holder = inflater.inflate(R.layout.fragment_settings2, container, false);
        unbinder = ButterKnife.bind(this, holder);
        return holder;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appSettings = activitySearch.getApp().getAppSettings();
    }

}
