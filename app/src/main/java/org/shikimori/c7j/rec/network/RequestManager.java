package org.shikimori.c7j.rec.network;

import android.widget.Toast;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.data.AccessToken;
import org.shikimori.c7j.rec.data.Related;
import org.shikimori.c7j.rec.data.Screenshot;
import org.shikimori.c7j.rec.data.animeinfo.AnimeInfo;
import org.shikimori.c7j.rec.data.animerates.AnimeRate;
import org.shikimori.c7j.rec.data.roles.Role;
import org.shikimori.c7j.rec.data.search.SearchResult;
import org.shikimori.c7j.rec.data.userrate.UserRate;
import org.shikimori.c7j.rec.data.userrate.UserRateSend;
import org.shikimori.c7j.rec.data.whoami.WhoAmI;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.ui.UIUtils;
import org.shikimori.c7j.rec.utils.L;
import org.shikimori.c7j.rec.utils.RxUtils;
import org.shikimori.c7j.rec.utils.SearchResultsFilter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


@SuppressWarnings("WeakerAccess")
public class RequestManager {

    /** To send requests that do not require authorization */
    private ShikimoriApi shikimoriApi = null;
    /** To send requests that require authorization */
    private ShikimoriApi shikimoriAuthorizedApi = null;
    /** Mechanism designed the retry failed requests */
    private ReconnectionMaster reconnectionMaster;
    /** Key that identifies our app when connecting to shikimori.org service */
    private String shikimoriAppId;

    /** Comes true if page1 or page2 genres request has failed */
    private boolean genresRequestFailed = false;
    /** Comes true when request page1 is complete */
    private boolean genresRequestPage1Complete = false;
    /** Comes true when request page2 is complete */
    private boolean genresRequestPage2Complete = false;
    /** Collects the data of genres pages requests while they are running */
    private List<SearchResult> genresAllSearchResults = new ArrayList<>();

    private Subscription subGenresPage1Request;
    private Subscription subGenresPage2Request;


    public RequestManager(String shikimoriAppId) {
        this.shikimoriAppId = shikimoriAppId;
        shikimoriApi = getShikimoriService();
        reconnectionMaster = new ReconnectionMaster();
    }


    public ShikimoriApi getAuthorizedShikimoriService(String username, String accessToken) {
        if (shikimoriAuthorizedApi == null) {
            shikimoriAuthorizedApi = ShikimoriAuthorizedService
                    .createShikimoriAuthorizedService( shikimoriAppId, username, accessToken );
        }
        return shikimoriAuthorizedApi;
    }


    public ShikimoriApi getShikimoriService() {
        if (shikimoriApi == null) {
            shikimoriApi = ShikimoriService.createShikimoriService(shikimoriAppId);
        }
        return shikimoriApi;
    }


    public void requestAccessToken(final ActivitySearch activitySearch, final String nickname, final String password) {
        GAnalytics.reportApiCall(ShikimoriApi.API_REQUEST_ACCESS_TOKEN);

        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getAccessToken(nickname, password)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<AccessToken>() {

                            @Override
                            public void onNext(AccessToken accessToken) {
                                activitySearch.onSuccessAccessToken(accessToken, nickname);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailedNoParams(ShikimoriApi.API_REQUEST_ACCESS_TOKEN, e.toString());
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_ACCESS_TOKEN + " " + e.toString());
                                activitySearch.onFailAccessToken();
                                isServiceUnavailable(activitySearch, e);
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestWhoAmI(final ActivitySearch activitySearch) {
        GAnalytics.reportApiCall(ShikimoriApi.API_REQUEST_WHOAMI);

        if (shikimoriAuthorizedApi == null) {
            shikimoriAuthorizedApi = getAuthorizedShikimoriService(
                    activitySearch.getAppSettings().readUserNickname(),
                    activitySearch.getAppSettings().readAccessToken()
            );
        }
        activitySearch.get_subscriptions().add(
                shikimoriAuthorizedApi
                        .getWhoAmI()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<WhoAmI>() {

                            @Override
                            public void onNext(WhoAmI whoAmI) {
                                activitySearch.onSuccessWhoAmI(whoAmI);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailedNoParams(ShikimoriApi.API_REQUEST_WHOAMI, e.toString());
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch, ShikimoriApi.REQUEST_WHOAMI, null);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_WHOAMI + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestSimilar(final ActivitySearch activitySearch, final String animeId, boolean isReconnecting) {
        activitySearch.searchResultsScrollPosition = 0; //new fragment opens from scroll position 0
        if (!isReconnecting) activitySearch.loadSearchResultsFragment(null);
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getSimilar(animeId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<SearchResult>>() {

                            @Override
                            public void onNext(List<SearchResult> similarAnimes) {
                                activitySearch.onSuccessSimilar(similarAnimes, false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailed(ShikimoriApi.API_REQUEST_SIMILAR, e.toString(), animeId);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch, ShikimoriApi.REQUEST_SIMILAR, animeId);
                                }
                                isServiceUnavailable(activitySearch, e);
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SIMILAR + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestRoles(final ActivitySearch activitySearch, final String animeId) {
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getRoles(animeId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<Role>>() {

                            @Override
                            public void onNext(List<Role> roles) {
                                activitySearch.onSuccessRoles(roles);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailed(ShikimoriApi.API_REQUEST_ROLES, e.toString(), animeId);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch, ShikimoriApi.REQUEST_ROLES, animeId);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_ROLES + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestAnimeInfo(final ActivitySearch activitySearch, final String animeId, boolean isReconnecting) {
        GAnalytics.reportApiCall(ShikimoriApi.API_REQUEST_ANIMEINFO);
        if (!isReconnecting) activitySearch.loadAnimeInfoFragment(null);
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getAnimeInfo(animeId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<AnimeInfo>() {

                            @Override
                            public void onNext(AnimeInfo animeInfo) {
                                activitySearch.onSuccessAnimeInfo(animeInfo);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailed(ShikimoriApi.API_REQUEST_ANIMEINFO, e.toString(), animeId);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch,
                                            ShikimoriApi.REQUEST_ANIMEINFO, animeId);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_ANIMEINFO + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestSearch(final ActivitySearch activitySearch, final String searchQuery) {
        GAnalytics.reportApiCallSearch();
        String season = SearchResultsFilter.getEnabledYears(activitySearch.getAppSettings());
        String type = SearchResultsFilter.getEnabledTypes(activitySearch.getAppSettings());
        String order = SearchResultsFilter.getOrderByMode(activitySearch.getAppSettings());
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getSearchResult(searchQuery, ShikimoriApi.SEARCH_QUERY_LIMIT, season, type, order, true)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<SearchResult>>() {

                            @Override
                            public void onNext(List<SearchResult> searchResults) {
                                activitySearch.onSuccessSearch(searchResults, false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportSearchRequestFailed(ShikimoriApi.API_REQUEST_SEARCH, e.toString(), searchQuery);
                                isServiceUnavailable(activitySearch, e);
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestSearchSubmit(final ActivitySearch activitySearch, final String query) {
        GAnalytics.reportApiCallSearch();
        String season = SearchResultsFilter.getEnabledYears(activitySearch.getAppSettings());
        String type = SearchResultsFilter.getEnabledTypes(activitySearch.getAppSettings());
        String order = SearchResultsFilter.getOrderByMode(activitySearch.getAppSettings());

        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getSearchResult(query, ShikimoriApi.SEARCH_QUERY_LIMIT, season, type, order, true)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<SearchResult>>() {

                            @Override
                            public void onNext(List<SearchResult> searchResults) {
                                activitySearch.onSuccessSearchSubmit(searchResults, false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportSearchRequestFailed(ShikimoriApi.API_REQUEST_SEARCH, e.toString(), query);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch, ShikimoriApi.REQUEST_SEARCH, query);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestCreateUserRate(final ActivitySearch activitySearch, final String userRateSend) {
        GAnalytics.reportApiCall(ShikimoriApi.API_REQUEST_CREATE_USER_RATE);
        if (shikimoriAuthorizedApi == null) {
            shikimoriAuthorizedApi = getAuthorizedShikimoriService(
                    activitySearch.getAppSettings().readUserNickname(),
                    activitySearch.getAppSettings().readAccessToken()
            );
        }
        activitySearch.get_subscriptions().add(
                shikimoriAuthorizedApi
                        .getCreateUserRate(new UserRateSend().fromGsonString(userRateSend))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserRate>() {

                            @Override
                            public void onNext(UserRate userRate) {
                                activitySearch.onSuccessCreateUserRate(userRate);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailed(
                                        ShikimoriApi.API_REQUEST_CREATE_USER_RATE,
                                        e.toString(),
                                        "" + new UserRateSend().fromGsonString(userRateSend).getTargetId()
                                );
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch,
                                            ShikimoriApi.REQUEST_CREATE_USER_RATE, userRateSend);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_CREATE_USER_RATE + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestAllAnimeRates(final ActivitySearch activitySearch, final String userId, final boolean clickSave) {
        final int ANIME_RATES_LIST_MAX_SIZE = 99999;
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getAllAnimeRates(userId, ANIME_RATES_LIST_MAX_SIZE)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<AnimeRate>>() {

                            @Override
                            public void onNext(List<AnimeRate> animeRates) {
                                activitySearch.onSuccessAllAnimeRates(animeRates, clickSave);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailedNoParams(ShikimoriApi.API_REQUEST_GET_ALL_ANIME_RATES, e.toString());
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    if (clickSave) {
                                        reconnectionMaster.reconnect(
                                                activitySearch, ShikimoriApi.REQUEST_GET_ALL_ANIME_RATES_SAVE, userId);
                                    } else {
                                        reconnectionMaster.reconnect(
                                                activitySearch, ShikimoriApi.REQUEST_GET_ALL_ANIME_RATES, userId);
                                    }
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_GET_ALL_ANIME_RATES + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }



    public void requestSearchByStatus(final ActivitySearch activitySearch, final String status) {
        activitySearch.loadSearchResultsFragment(null);
        String type = SearchResultsFilter.getEnabledTypes(activitySearch.getAppSettings());
        String order = SearchResultsFilter.getOrderByMode(activitySearch.getAppSettings());

        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getSearchByStatus(
                                true,
                                ShikimoriApi.SEARCH_QUERY_LIMIT,
                                order,
                                type,
                                status)

                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<SearchResult>>() {

                            @Override
                            public void onNext(List<SearchResult> searchResults) {
                                activitySearch.onSuccessSearchSubmit(searchResults, true);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch,
                                            ShikimoriApi.REQUEST_SEARCH_BY_STATUS, status);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH_BY_STATUS + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }



    public void requestSearchByGenre(final ActivitySearch activitySearch, final String genreId) {
        activitySearch.loadSearchResultsFragment(null);
        String season = SearchResultsFilter.getEnabledYears(activitySearch.getAppSettings());
        String type = SearchResultsFilter.getEnabledTypes(activitySearch.getAppSettings());
        String order = SearchResultsFilter.getOrderByMode(activitySearch.getAppSettings());

        genresRequestFailed = false;
        genresRequestPage1Complete = false;
        genresRequestPage2Complete = false;
        genresAllSearchResults = new ArrayList<>();
        if (subGenresPage1Request != null ) subGenresPage1Request.unsubscribe();
        if (subGenresPage2Request != null ) subGenresPage2Request.unsubscribe();

        subGenresPage1Request = shikimoriApi
                .getSearchByGenre(
                        true,
                        genreId,
                        ShikimoriApi.SEARCH_QUERY_LIMIT,
                        1,
                        season,
                        type,
                        order)

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SearchResult>>() {

                    @Override
                    public void onNext(List<SearchResult> searchResults) {
                        completeGenresRequest(activitySearch, searchResults, 1);
                    }

                    @Override
                    public void onError(Throwable e) {
                        RxUtils.logOnErrorStackTrace(e);
                        completeGenresRequest(activitySearch, null, 1);
                        L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH_BY_GENRE + " " + e.toString());
                    }

                    @Override
                    public void onCompleted() { /* Do Nothing */ }
                });

        subGenresPage2Request = shikimoriApi
                .getSearchByGenre(
                        true,
                        genreId,
                        ShikimoriApi.SEARCH_QUERY_LIMIT,
                        2,
                        season,
                        type,
                        order)

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SearchResult>>() {

                    @Override
                    public void onNext(List<SearchResult> searchResults) {
                        completeGenresRequest(activitySearch, searchResults, 2);
                    }

                    @Override
                    public void onError(Throwable e) {
                        RxUtils.logOnErrorStackTrace(e);
                        completeGenresRequest(activitySearch, null, 2);
                        L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH_BY_GENRE + " " + e.toString());
                    }

                    @Override
                    public void onCompleted() { /* Do Nothing */ }
                });

        activitySearch.get_subscriptions().add(subGenresPage1Request);
        activitySearch.get_subscriptions().add(subGenresPage2Request);
    }



    private void completeGenresRequest(ActivitySearch activitySearch, List<SearchResult> searchResults, int page) {

        if (page == 1) genresRequestPage1Complete = true;
        if (page == 2) genresRequestPage2Complete = true;

        if (searchResults != null) {
            if (page == 1 && !genresAllSearchResults.isEmpty()) {
                searchResults.addAll(genresAllSearchResults);
                genresAllSearchResults = searchResults;
            } else genresAllSearchResults.addAll(searchResults);
        } else genresRequestFailed = true;

        if (genresRequestPage1Complete && genresRequestPage2Complete && genresRequestFailed) {
            activitySearch.onBackPressed();
            UIUtils.showCustomToast(activitySearch, R.layout.toast_failed_to_connect, Toast.LENGTH_SHORT);
            return;
        } else if (genresRequestPage1Complete && genresRequestPage2Complete) {
            activitySearch.onSuccessSearchSubmit(genresAllSearchResults, false);
        }
    }



    public void requestSearchByType(final ActivitySearch activitySearch, final String type) {
        activitySearch.loadSearchResultsFragment(null);
        String season = SearchResultsFilter.getEnabledYears(activitySearch.getAppSettings());
        String order = SearchResultsFilter.getOrderByMode(activitySearch.getAppSettings());

        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getSearchByType(
                                true,
                                type,
                                ShikimoriApi.SEARCH_QUERY_LIMIT,
                                season,
                                order)

                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<SearchResult>>() {

                            @Override
                            public void onNext(List<SearchResult> searchResults) {
                                activitySearch.onSuccessSearchSubmit(searchResults, false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch,
                                            ShikimoriApi.REQUEST_SEARCH_BY_TYPE, type);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH_BY_TYPE + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }


    public void requestSearchRandom(final ActivitySearch activitySearch) {
        activitySearch.loadSearchResultsFragment(null);
        String type = ShikimoriApi.KIND_TV +"," + ShikimoriApi.KIND_MOVIE;
        String page = "" + ( new Random().nextInt(15) + 3 ); //random from 50-item pages 3-18 of order by popularity
        String order = SearchResultsFilter.getOrderByMode(activitySearch.getAppSettings());

        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getRandomSearch(
                                true,
                                type,
                                ShikimoriApi.SEARCH_QUERY_LIMIT,
                                page,
                                order)

                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<SearchResult>>() {

                            @Override
                            public void onNext(List<SearchResult> searchResults) {
                                activitySearch.onSuccessSearchSubmit(searchResults, false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch,
                                            ShikimoriApi.REQUEST_SEARCH_RANDOM, null);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SEARCH_RANDOM + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }



    public void requestRelated(final ActivitySearch activitySearch, final String animeId) {
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getRelated(animeId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<Related>>() {

                            @Override
                            public void onNext(List<Related> related) {
                                activitySearch.onSuccessRelated(related);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
//                                GAnalytics.reportRequestFailed(ShikimoriApi.API_REQUEST_ROLES, e.toString(), animeId);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch, ShikimoriApi.REQUEST_RELATED, animeId);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_RELATED + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }



    public void requestScreenshots(final ActivitySearch activitySearch, final String animeId) {
        activitySearch.get_subscriptions().add(
                shikimoriApi
                        .getScreenshots(animeId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<Screenshot>>() {

                            @Override
                            public void onNext(List<Screenshot> roles) {
                                activitySearch.onSuccessScreenshots(roles);
                            }

                            @Override
                            public void onError(Throwable e) {
                                RxUtils.logOnErrorStackTrace(e);
                                GAnalytics.reportRequestFailed(ShikimoriApi.API_REQUEST_ROLES, e.toString(), animeId);
                                if (!isServiceUnavailable(activitySearch, e)) {
                                    reconnectionMaster.reconnect(activitySearch, ShikimoriApi.REQUEST_ROLES, animeId);
                                }
                                L.n("ERROR: " + ShikimoriApi.API_REQUEST_SCREENSHOTS + " " + e.toString());
                            }

                            @Override
                            public void onCompleted() { /* Do Nothing */ }
                        })
        );
    }



    private boolean isServiceUnavailable(ActivitySearch activitySearch, Throwable e) {
        if (e.toString().contains("502")) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.GERMANY);
            GAnalytics.reportUnexpectedError("502 Service unavailable", "at: " +
                    dateFormat.format(System.currentTimeMillis()));
            UIUtils.showCustomToast(activitySearch, R.layout.toast_service_unavailable, Toast.LENGTH_SHORT);
            return true;
        } else return false;
    }

}
