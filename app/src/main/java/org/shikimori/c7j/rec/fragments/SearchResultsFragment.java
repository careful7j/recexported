package org.shikimori.c7j.rec.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.ui.UIUtils;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.utils.ResponseInfo;
import org.shikimori.c7j.rec.data.search.SearchResult;
import org.shikimori.c7j.rec.ui.AdapterSearchResults;

import java.util.List;

import butterknife.ButterKnife;


public class SearchResultsFragment extends ABaseFragment {

    private GridView grid;
    private AdapterSearchResults adapter;
    private View holder;

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.SEARCH_RESULTS);
    public FragmentInfo getFragmentInfo() { return fragmentInfo; }


    @SuppressWarnings("unchecked")
    public void updateGrid(FragmentInfo tempFragmentInfo) {
        try {
            if ( tempFragmentInfo.getFragmentType() == FragmentInfo.SEARCH_RESULTS ) {
                List<ResponseInfo> responses = tempFragmentInfo.getResponses();
                for ( ResponseInfo responseInfo : responses ) {
                    if ( responseInfo.getResponseType() == ResponseInfo.GET_SEARCH_LIST ) {
                        updateGrid( (List<SearchResult>) responseInfo.getApiResponse() );
                    }
                }
            }
        } catch (Exception e) {
            // In case search results are corrupt, silently return one screen back (crashlytics: 28 occasions a week )
            // TODO: Investigate this when have time (probably google analytics may help)
            if ( activitySearch != null ) activitySearch.onBackPressed();
        }
    }



    public void updateGrid(final List<SearchResult> searchResults) {
        holder.findViewById(R.id.loading_circle).setVisibility(View.INVISIBLE);
        holder.findViewById(R.id.search_grid).setVisibility(View.VISIBLE);
        populateGrid(searchResults);
        grid.setSelection(activitySearch.searchResultsScrollPosition);
        activitySearch.searchResultsScrollPosition = 0;
    }


    private void populateGrid(final List<SearchResult> searchResults) {
        adapter = new AdapterSearchResults(searchResults);
        grid = (GridView) holder.findViewById(R.id.search_grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if ( activitySearch != null &&
                        activitySearch.isInternetConnected() &&
                        searchResults != null &&
                        !searchResults.isEmpty())
                {
                    SearchResult searchResult = searchResults.get(position);
                    if (searchResult != null) {
                        Integer animeId = searchResult.getId();
                        if (animeId != null) {
                            if (grid != null) activitySearch.searchResultsScrollPosition = grid.getFirstVisiblePosition();
                            activitySearch.getRm().requestAnimeInfo((ActivitySearch) getActivity(), animeId.toString(), false);
                            activitySearch.getRm().requestRoles((ActivitySearch) getActivity(), animeId.toString());
                            activitySearch.getRm().requestRelated((ActivitySearch) getActivity(), animeId.toString());
                            activitySearch.getRm().requestScreenshots((ActivitySearch) getActivity(), animeId.toString());
                            activitySearch.lastAnimeId = "" + animeId;
                        }
                    }
                }
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        holder = inflater.inflate(R.layout.fragment_searchgrid, container, false);
        unbinder = ButterKnife.bind(this, holder);
        return holder;
    }
}
