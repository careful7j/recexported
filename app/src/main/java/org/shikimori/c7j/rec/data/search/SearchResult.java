package org.shikimori.c7j.rec.data.search;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/** IMPORTANT: Both Search and Similar Anime use this datastructure */
@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class SearchResult implements Serializable {

    private Integer id;
    private String name;
    private String russian;
    private Image image;
    private String url;
    private String kind;
    private String status;
    private Integer episodes;
    @SerializedName("episodes_aired")
    private Integer episodesAired;
    @SerializedName("aired_on")
    private String airedOn;
    @SerializedName("released_on")
    private String releasedOn;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public String getRussian() {
        return russian;
    }

    
    public void setRussian(String russian) {
        this.russian = russian;
    }

    
    public Image getImage() {
        return image;
    }

    
    public void setImage(Image image) {
        this.image = image;
    }

    
    public String getUrl() {
        return url;
    }

    
    public void setUrl(String url) {
        this.url = url;
    }

    
    public String getKind() {
        return kind;
    }

    
    public void setKind(String kind) {
        this.kind = kind;
    }

    
    public String getStatus() {
        return status;
    }

    
    public void setStatus(String status) {
        this.status = status;
    }

    
    public Integer getEpisodes() {
        return episodes;
    }

    
    public void setEpisodes(Integer episodes) {
        this.episodes = episodes;
    }

    
    public Integer getEpisodesAired() {
        return episodesAired;
    }

    
    public void setEpisodesAired(Integer episodesAired) {
        this.episodesAired = episodesAired;
    }

    
    public String getAiredOn() {
        return airedOn;
    }

    
    public void setAiredOn(String airedOn) {
        this.airedOn = airedOn;
    }

    
    public String getReleasedOn() {
        return releasedOn;
    }

    
    public void setReleasedOn(String releasedOn) {
        this.releasedOn = releasedOn;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", russian='" + russian + '\'' +
                ", image=" + image +
                ", url='" + url + '\'' +
                ", kind='" + kind + '\'' +
                ", status='" + status + '\'' +
                ", episodes=" + episodes +
                ", episodesAired=" + episodesAired +
                ", airedOn='" + airedOn + '\'' +
                ", releasedOn='" + releasedOn + '\'' +
                ", additionalProperties=" + additionalProperties +
                "}\n";
    }
}
