package org.shikimori.c7j.rec.network;

import android.os.Handler;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.services.GAnalytics;
import org.shikimori.c7j.rec.utils.L;


/**
 * In case of occasional bad server response or request timeout we retry our request up to 3 times.
 * This job is served by this very class.
 */
public class ReconnectionMaster {

    private final int RECONNECT_MAX_ATTEMPTS = 2;
    private final int RECONNECT_RETRY_INTERVAL = 200;

    /** One independent counter per each request */
    private int[] reconnectCounters = new int[ShikimoriApi.TOTAL_NUMBER_OF_REQUESTS];

    public void reconnect(
            final ActivitySearch activitySearch,
            final int requestId,
            final String queryParameter
    ) {

        if ( requestId > ShikimoriApi.TOTAL_NUMBER_OF_REQUESTS ) {
            GAnalytics.reportBadQueryId(requestId);
            return; //ignore unknown requests
        }

        if ( reconnectCounters[requestId] > RECONNECT_MAX_ATTEMPTS ) {

            reconnectCounters[requestId] = 0;
            switch (requestId) {

                case ShikimoriApi.REQUEST_ROLES :
                case ShikimoriApi.REQUEST_WHOAMI :
                case ShikimoriApi.REQUEST_GET_ALL_ANIME_RATES :
                case ShikimoriApi.REQUEST_GET_ALL_ANIME_RATES_SAVE :
                case ShikimoriApi.REQUEST_CREATE_USER_RATE :
                case ShikimoriApi.REQUEST_RELATED :
                case ShikimoriApi.REQUEST_SCREENSHOTS :
                    break;
                // All the other shall close last opened fragment by calling OnBackPressed():
                default : activitySearch.onBackPressed();
            }

        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    reconnectCounters[requestId]++;
                    switch (requestId) {

                        case ShikimoriApi.REQUEST_SIMILAR :
                            activitySearch.getRm().requestSimilar(activitySearch, queryParameter, true);
                            L.t("Reconnecting to similar (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_SEARCH :
                            activitySearch.getRm().requestSearchSubmit(activitySearch, queryParameter);
                            L.t("Reconnecting to searchSubmit (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_ROLES :
                            activitySearch.getRm().requestRoles(activitySearch, queryParameter);
                            L.t("Reconnecting to roles (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_ANIMEINFO :
                            activitySearch.getRm().requestAnimeInfo(activitySearch, queryParameter, true);
                            L.t("Reconnecting to animeInfo (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_WHOAMI:
                            activitySearch.getRm().requestWhoAmI(activitySearch);
                            L.t("Reconnecting to whoAmI (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_GET_ALL_ANIME_RATES :
                            activitySearch.getRm().requestAllAnimeRates(activitySearch, queryParameter, false);
                            L.t("Reconnecting to allAnimeRates (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_GET_ALL_ANIME_RATES_SAVE:
                            activitySearch.getRm().requestAllAnimeRates(activitySearch, queryParameter, true);
                            L.t("Reconnecting to allAnimeRates -save (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_CREATE_USER_RATE:
                            activitySearch.getRm().requestCreateUserRate(activitySearch, queryParameter);
                            L.t("Reconnecting to createUserRate (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_SEARCH_BY_STATUS :
                            activitySearch.getRm().requestSearchByStatus(activitySearch, queryParameter);
                            L.t("Reconnecting to searchByStatus (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_SEARCH_BY_GENRE :
                            activitySearch.getRm().requestSearchByGenre(activitySearch, queryParameter);
                            L.t("Reconnecting to searchByGenre (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_SEARCH_BY_TYPE :
                            activitySearch.getRm().requestSearchByType(activitySearch, queryParameter);
                            L.t("Reconnecting to searchByType (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_SEARCH_RANDOM :
                            activitySearch.getRm().requestSearchRandom(activitySearch);
                            L.t("Reconnecting to searchRandom (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_RELATED :
                            activitySearch.getRm().requestRelated(activitySearch, queryParameter);
                            L.t("Reconnecting to related (" + reconnectCounters[requestId] + ")");
                            break;

                        case ShikimoriApi.REQUEST_SCREENSHOTS :
                            activitySearch.getRm().requestScreenshots(activitySearch, queryParameter);
                            L.t("Reconnecting to screenshots (" + reconnectCounters[requestId] + ")");
                            break;

                    }
                }
            }, RECONNECT_RETRY_INTERVAL);
        }
    }

}
