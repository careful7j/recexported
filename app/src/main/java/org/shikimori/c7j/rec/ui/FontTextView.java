package org.shikimori.c7j.rec.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import org.shikimori.c7j.rec.MyApp;


/** TextView with custom font used by default */
public class FontTextView extends android.support.v7.widget.AppCompatTextView {


    public FontTextView( Context context ) {
        super( context );
        setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL );
    }


    public FontTextView( Context context, AttributeSet attrs ) {
        super( context, attrs );
        setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL );
    }


    public FontTextView( Context context, AttributeSet attrs, int defStyle ) {
        super( context, attrs, defStyle );
        setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL );
    }

}
