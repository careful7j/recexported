package org.shikimori.c7j.rec.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.R;

import butterknife.Unbinder;


public class ABaseFragment extends Fragment {

    /** Activity to display fragment in */
    protected ActivitySearch activitySearch = null;
    /** Stores fragment information while fragment views are initializing */
    protected FragmentInfo tempFragmentInfo = null;
    /** On fragment's views complete initializing callback to activity */
    protected IonFragmentCompleteLoading onFragmentCompleteLoading;

    public Unbinder unbinder;

    public void setTempFragmentInfo( FragmentInfo tempFragmentInfo ) { this.tempFragmentInfo = tempFragmentInfo; };

    public ActivitySearch getActivitySearch() { return activitySearch; };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        activitySearch = null;
        onFragmentCompleteLoading = null;
        if ( unbinder != null ) unbinder.unbind();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activitySearch = (ActivitySearch) getActivity();
        FrameLayout coreLayout = (FrameLayout) activitySearch.findViewById(R.id.fragment_container1);
        coreLayout.requestFocus();
        try {
            this.onFragmentCompleteLoading = activitySearch;
            this.onFragmentCompleteLoading.onFragmentCompleteLoading(tempFragmentInfo);
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activitySearch.toString() + " must implement IonFragmentCompleteLoading");
        }
    }
}
