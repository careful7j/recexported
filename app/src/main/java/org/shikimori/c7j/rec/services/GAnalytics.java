package org.shikimori.c7j.rec.services;

import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.shikimori.c7j.rec.BuildConfig;
import org.shikimori.c7j.rec.R;
import org.shikimori.c7j.rec.network.ShikimoriApi;


@SuppressWarnings("WeakerAccess")
public class GAnalytics {

    private static final String CAT_API_CALL = "Api Call";
    private static final String CAT_TO_SHIKIMORI = "To Shikimori";
    private static final String CAT_SUGGEST = "Suggest Clicked";
    private static final String CAT_ERROR = "Error at: ";
    private static final String CAT_RATE_US = "Rate us";
    private static final String CAT_SHIKIMORI_CLUB_VISITED = "Shikimori club visited";
    private static final String CAT_SHIKIMORI_ANIME_NEWS_VISITED = "Shikimori anime news visited";
    private static final String CAT_RECONNECTION_MASTER = "Reconnection master";
    private static final String CAT_MIKUO_VISITED = "Mikuo visited ";
    private static final String CAT_ANIME_LIST = "Animelist";
    private static final String CAT_UNEXPECTED_ERRORS = "Other errors";
    private static final String CAT_MENU_ITEM_SELECTED = "Menu item selected";
    private static final String CAT_TRAILER_WATCHED = "Trailer watched";
    private static final String CAT_RELATED_VISITED = "Related visited";

    public static final String ACTION_CLICKED = "clicked";
    public static final String ACTION_VISITED = "visited";

    public static final String ACTION_FROM_STARTING_FRAGMENT = "from homepage";
    public static final String ACTION_FROM_SETTINGS_FRAGMENT = "from settings";

    public static final String ACTION_USER_GOES_TO_SHIKIMORI = "User goes to shikimori";
    public static final String ACTION_SUGGEST_ANIME_CLICKED = "Suggest anime clicked";
    public static final String ACTION_BAD_QUERY_ID = "Bad query Id: ";
    public static final String ACTION_ID = "id: ";
    public static final String ACTION_QUERY = "query: ";
    public static final String ACTION_X10 = " (10)";

    public static final String ACTION_ITEM_AIRING = "Airing";
    public static final String ACTION_ITEM_ANIME_MOVIES = "Anime movies";
    public static final String ACTION_ITEM_ANNOUNCED = "Announced";
    public static final String ACTION_ITEM_LATEST = "Latest";
    public static final String ACTION_ITEM_RANDOM = "Random";


    /** GAnalytics tracker by itself */
    private static Tracker tracker;
    /** False when application launched in Debug mode */
    private static boolean enabled;
        /** Every 10 search requests analytics report is sent */
    public static long searchRequestsCounter = 0;


    public static Tracker initialize(Context context) {
        if (tracker == null) {
            com.google.android.gms.analytics.GoogleAnalytics analytics =
                    com.google.android.gms.analytics.GoogleAnalytics.getInstance(context);
            tracker = analytics.newTracker(
                    context.getResources().getString(R.string.google_analytics_api_key));
        }
        enabled = !BuildConfig.DEBUG;
        return tracker;
    }


    public static void reportToShikimori(String label) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_TO_SHIKIMORI)
                .setAction(ACTION_USER_GOES_TO_SHIKIMORI)
                .setLabel(label)
                .build());
    }


    public static void reportSuggest(String label) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_SUGGEST)
                .setAction(ACTION_SUGGEST_ANIME_CLICKED)
                .setLabel(label)
                .build());
    }


    public static void reportRequestFailed(String requestName, String errorContent, String animeId ) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_ERROR + requestName)
                .setAction(errorContent)
                .setLabel(ACTION_ID + animeId)
                .build());
    }

    public static void reportRequestFailedNoParams(String requestName, String errorContent) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_ERROR + requestName)
                .setAction(errorContent)
                .build());
    }

    public static void reportSearchRequestFailed(String requestName, String errorContent, String searchQuery ) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_ERROR + requestName)
                .setAction(errorContent)
                .setLabel(ACTION_QUERY + searchQuery)
                .build());
    }

    public static void reportRateUs() {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_RATE_US)
                .build());
    }

    public static void reportShikimoriClubVisited() {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_SHIKIMORI_CLUB_VISITED)
                .build());
    }

    public static void reportAnimeNewsVisited() {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_SHIKIMORI_ANIME_NEWS_VISITED)
                .build());
    }

    public static void reportBadQueryId(int id) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_RECONNECTION_MASTER)
                .setAction(ACTION_BAD_QUERY_ID + id)
                .build());
    }

    public static void reportApiCallSearch() {
        if (!enabled) return;
        searchRequestsCounter++;
        if (searchRequestsCounter % 10 == 0) {
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(CAT_API_CALL)
                    .setAction(ShikimoriApi.API_REQUEST_SEARCH + ACTION_X10)
                    .build());
        }
    }

    public static void reportApiCall(String apiCall) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_API_CALL)
                .setAction(apiCall)
                .build());
    }

    public static void reportAuthorFromStartingFragmentVisited(String action) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_MIKUO_VISITED)
                .setAction(action)
                .build());
    }

    public static void reportAnimeListVisited(String action) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_ANIME_LIST)
                .setAction(action)
                .build());
    }

    public static void reportUnexpectedError(String action, String content) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_UNEXPECTED_ERRORS)
                .setAction(action)
                .setLabel(content)
                .build());
    }

    public static void reportMenuItemSelected(String item) {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_MENU_ITEM_SELECTED)
                .setAction(item)
                .build());
    }

    public static void reportTrailerWatched() {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_TRAILER_WATCHED)
                .build());
    }

    public static void reportRelatedVisited() {
        if (!enabled) return;
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(CAT_RELATED_VISITED)
                .build());
    }
}
