package org.shikimori.c7j.rec;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import android.os.Handler;

import org.shikimori.c7j.rec.data.AccessToken;
import org.shikimori.c7j.rec.data.Related;
import org.shikimori.c7j.rec.data.Screenshot;
import org.shikimori.c7j.rec.data.animeinfo.AnimeInfo;
import org.shikimori.c7j.rec.data.animerates.AnimeRate;
import org.shikimori.c7j.rec.data.roles.Role;
import org.shikimori.c7j.rec.data.search.SearchResult;
import org.shikimori.c7j.rec.data.userrate.UserRate;
import org.shikimori.c7j.rec.data.userrate.UserRateSend;
import org.shikimori.c7j.rec.data.whoami.WhoAmI;
import org.shikimori.c7j.rec.fragments.AnimeInfoFragment;
import org.shikimori.c7j.rec.fragments.AuthFragment;
import org.shikimori.c7j.rec.fragments.EmptyFragment;
import org.shikimori.c7j.rec.fragments.IonFragmentCompleteLoading;
import org.shikimori.c7j.rec.fragments.PrefsFragment;
import org.shikimori.c7j.rec.fragments.SearchResultsFragment;
import org.shikimori.c7j.rec.fragments.StartingFragment;
import org.shikimori.c7j.rec.fragments.TabHolderFragment;
import org.shikimori.c7j.rec.network.ShikimoriService;
import org.shikimori.c7j.rec.services.AFirebaseEvent;
import org.shikimori.c7j.rec.ui.UIUtils;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.utils.L;
import org.shikimori.c7j.rec.utils.ResponseInfo;
import org.shikimori.c7j.rec.utils.SearchResultsFilter;

import java.util.ArrayList;
import java.util.List;


//import com.google.firebase.analytics.FirebaseAnalytics;


public class ActivitySearch extends CoreActivity implements IonFragmentCompleteLoading {

    public String lastAnimeId = "";
    public static String lastSearchQuery = "";

    private SimpleCursorAdapter searchAdapter;
    private ArrayList<String> suggestions = new ArrayList<>();

    private String SEARCH_RESULTS_COLUMN = "SEARCH_RESULTS";

    private List<SearchResult> allSearchResults = new ArrayList<>(100);
    private SearchView searchView = null;

    public List<AnimeRate> animeRates = null;
    public String tempUserRateId = null;
    public int searchResultsScrollPosition = 0;

    private int mainContainerFragmentId;
    private FragmentManager fm;

    private AuthFragment authFragment;
    private AnimeInfoFragment animeInfoFragment;
    private SearchResultsFragment searchResultsFragment;
    private TabHolderFragment tabHolderFragment;

    public SearchView getSearchView() { return searchView; };


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        AFirebaseEvent.init(FirebaseAnalytics.getInstance(this));
//        AFirebaseEvent.appLaunched();

        getSupportActionBar().setDisplayShowTitleEnabled(false); //NPE Never ever happens
        setContentView(R.layout.activity_search);
        loadStartingFragment(null);

        String userId = appSettings.readUserId();
        if (!userId.isEmpty()) getRm().requestAllAnimeRates(ActivitySearch.this, userId, false);

        handleShortcutIntents();
        //  throw new RuntimeException("Test Crash Anime Rec"); // Crashlytics check
    }



    public void onSuccessSimilar(List<SearchResult> similarAnimes, boolean ignoreIsOngoing) {
        if (similarAnimes == null || similarAnimes.isEmpty()) {
            UIUtils.showCustomToast(ActivitySearch.this, R.layout.toast_similar_not_found, Toast.LENGTH_LONG);
            onBackPressed();
            return;
        }
        List<SearchResult> filteredList = SearchResultsFilter.applyAllFilters(appSettings, similarAnimes, ignoreIsOngoing);
        if ( searchResultsFragment != null ) {
            searchResultsFragment.updateGrid(filteredList);
        }
        ResponseInfo responseInfo = new ResponseInfo(ResponseInfo.GET_SEARCH_LIST, filteredList);
        searchResultsFragment.getFragmentInfo().addResponse(responseInfo);
        L.n("Similar success");
    }



    public void onSuccessRoles(List<Role> roles) {
        animeInfoFragment.updateCharacters(roles);
        ResponseInfo responseInfo = new ResponseInfo(ResponseInfo.GET_CHARACTERS_LIST, roles);
        animeInfoFragment.getFragmentInfo().addResponse(responseInfo);
        L.n("Roles success");
    }



    public void onSuccessAnimeInfo(AnimeInfo animeInfo) {
        animeInfoFragment.updateData(animeInfo);
        ResponseInfo responseInfo = new ResponseInfo(ResponseInfo.GET_ANIMEINFO, animeInfo);
        animeInfoFragment.getFragmentInfo().addResponse(responseInfo);
        L.n("AnimeInfo success");
    }


    public void onSuccessSearch(List<SearchResult> searchResults, boolean ignoreIsOngoing) {
        List<SearchResult> filteredList = SearchResultsFilter
                .applyAllFilters(appSettings, searchResults, ignoreIsOngoing);
        allSearchResults.addAll(filteredList);
        suggestions = SearchResultsFilter.getSearchSuggestions(filteredList, lastSearchQuery);
        populateSearchSuggestionsAdapter();
    }


    public void onSuccessSearchSubmit(List<SearchResult> searchResults, boolean ignoreIsOngoing) {
        if (searchResults == null || searchResults.isEmpty()) {
            UIUtils.showCustomToast( ActivitySearch.this, R.layout.toast_empty_search_results, Toast.LENGTH_LONG);
            onBackPressed();
            return;
        }
        List<SearchResult> filteredList = SearchResultsFilter.applyAllFilters(appSettings, searchResults, ignoreIsOngoing);
        if ( searchResultsFragment != null ) {
            searchResultsFragment.updateGrid(filteredList);
        }
        ResponseInfo responseInfo = new ResponseInfo(ResponseInfo.GET_SEARCH_LIST, filteredList);
        searchResultsFragment.getFragmentInfo().addResponse(responseInfo);
        L.n("SearchSubmit success");
    }


    public void onSuccessAccessToken(AccessToken accessToken, String nickname) {
        L.n("Access token received: " + accessToken.getApiAccessToken());
        if (accessToken.getApiAccessToken() != null) {
            appSettings.writeAccessToken(accessToken.getApiAccessToken());
            appSettings.writeUserNickname(nickname);
            requestManager.requestWhoAmI(this);
            onBackPressed();
        } else {
            UIUtils.showCustomToast(ActivitySearch.this, R.layout.toast_incorrect_password, Toast.LENGTH_SHORT);
            if (authFragment != null) {
                if (authFragment.authProgressBar != null) { authFragment.authProgressBar.setVisibility(View.INVISIBLE); }
                if (authFragment.btnLogin != null) { authFragment.btnLogin.setEnabled(true); }
            }
        }
    }


    public void onFailAccessToken() {
        L.n("Token fail");
        UIUtils.showCustomToast(this, R.layout.toast_failed_to_connect, Toast.LENGTH_SHORT);
        if (authFragment != null) {
            if (authFragment.authProgressBar != null) { authFragment.authProgressBar.setVisibility(View.INVISIBLE); }
            if (authFragment.btnLogin != null) { authFragment.btnLogin.setEnabled(true); }
        }
    }


    public void onSuccessWhoAmI(WhoAmI whoAmI) {
        L.n("Who I am?");
        if (whoAmI.getId() != null && whoAmI.getId() != 0) {
            L.n("User id received: " + whoAmI.getId());
            appSettings.writeUserId("" + whoAmI.getId());
        }
        if (    whoAmI.getNickname() != null &&
                !whoAmI.getNickname().isEmpty() &&
                !whoAmI.getNickname().equals("null"))
        {
            L.n("Nickname received: " + whoAmI.getNickname());
            appSettings.writeUserNickname("" + whoAmI.getNickname());
            requestManager.requestAllAnimeRates(ActivitySearch.this, "" + whoAmI.getId(), true);
        }
    }


    public void onSuccessAllAnimeRates(List<AnimeRate> animeRates, final boolean clickSave) {
        this.animeRates = animeRates;
        if (animeInfoFragment == null) return;
        if (clickSave) {
            animeInfoFragment.btnSave.callOnClick();
        } else {
            animeInfoFragment.updateSaveButton();
        }
        L.n("AllAnimeRates success");
    }


    public void onSuccessCreateUserRate(UserRate userRate) {
        tempUserRateId = "" + userRate.getId();
        animeInfoFragment.loadingCircleBtn.setVisibility(View.GONE);
        animeInfoFragment.btnSave.setVisibility(View.GONE);
        animeInfoFragment.btnDone.setVisibility(View.VISIBLE);
        animeInfoFragment.btnDone.setText(animeInfoFragment.strStatusPlanned);
        AnimeRate animeRate = new AnimeRate(userRate.getTargetId(), AnimeInfoFragment.QUERY_STATUS_PLANNED);
        animeRates.add(animeRate);
        L.t("Created user rate: " +  userRate.getStatus() + " " + userRate.getCreatedAt() + " " + userRate.getId());
    }


    public void onSuccessRelated(List<Related> related) {
        animeInfoFragment.updateRelated(related);
        ResponseInfo responseInfo = new ResponseInfo(ResponseInfo.GET_RELATED_LIST, related);
        animeInfoFragment.getFragmentInfo().addResponse(responseInfo);
        L.n("Related success");
    }


    public void onSuccessScreenshots(List<Screenshot> screenshots) {
        animeInfoFragment.updateScreenshots(screenshots);
        ResponseInfo responseInfo = new ResponseInfo(ResponseInfo.GET_SCREENSHOTS, screenshots);
        animeInfoFragment.getFragmentInfo().addResponse(responseInfo);
        L.n("Screenshots success");
    }


    public void loadAuthFragment(FragmentInfo fragmentInfo) {
        final AuthFragment authFragment = new AuthFragment();
        this.authFragment = authFragment;
        fm = getFragmentManager();
        mainContainerFragmentId = R.id.fragment_container1;
        fm.beginTransaction()
                .replace(mainContainerFragmentId, authFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        if (getSupportActionBar() != null) { getSupportActionBar().hide(); }
        if (fragmentInfo == null) {
            backStackUtils.getBackStack().addFirst(authFragment.getFragmentInfo());
        }
    }



    public void loadStartingFragment(FragmentInfo fragmentInfo) {
        final StartingFragment startingFragment = new StartingFragment();
        fm = getFragmentManager();
        mainContainerFragmentId = R.id.fragment_container1;
        fm.beginTransaction()
                .replace(mainContainerFragmentId, startingFragment)
                .commit();

        if (fragmentInfo == null) {
            backStackUtils.getBackStack().addFirst(startingFragment.getFragmentInfo());
        }
    }


    public void loadTabHolderFragment(FragmentInfo fragmentInfo) {
        tabHolderFragment = new  TabHolderFragment();
        fm = getFragmentManager();
        mainContainerFragmentId = R.id.fragment_container1;
        fm.beginTransaction()
                .replace(mainContainerFragmentId, tabHolderFragment)
                .commit();

        if (searchView != null) searchView.setQuery("", false);
        if (getSupportActionBar() != null) { getSupportActionBar().hide(); }
        if (fragmentInfo == null) {
            backStackUtils.getBackStack().addFirst(tabHolderFragment.getFragmentInfo());
        }
    }



    public void loadEmptyFragment(FragmentInfo fragmentInfo) {
        final EmptyFragment emptyFragment = new EmptyFragment();
        fm = getFragmentManager();
        mainContainerFragmentId = R.id.fragment_container1;
        fm.beginTransaction()
                .add(mainContainerFragmentId, emptyFragment)
                .commit();

        if (fragmentInfo == null) {
            backStackUtils.getBackStack().addFirst(emptyFragment.getFragmentInfo());
        }
    }



    public void loadPrefsFragment(FragmentInfo fragmentInfo) {
        final PrefsFragment prefsFragment = new PrefsFragment();
        fm = getFragmentManager();
        mainContainerFragmentId = R.id.fragment_container1;
        fm.beginTransaction()
                .replace(mainContainerFragmentId, prefsFragment)
                .commit();

        if (fragmentInfo == null) {
            backStackUtils.getBackStack().addFirst(prefsFragment.getFragmentInfo());
        }
    }



    public void loadAnimeInfoFragment(FragmentInfo tempFragmentInfo) {
        final AnimeInfoFragment animeInfoFragment = new AnimeInfoFragment();
        this.animeInfoFragment = animeInfoFragment;
        if (tempFragmentInfo == null ) {
            backStackUtils.getBackStack().addFirst(animeInfoFragment.getFragmentInfo());
        } else {
            animeInfoFragment.setTempFragmentInfo(tempFragmentInfo);
            if (tempFragmentInfo.getResponses().isEmpty()) {
                onBackPressed();    //This fragment hasn't loaded before app was closed last time
                return;
            }
        }
        fm.beginTransaction()
                .replace(mainContainerFragmentId, animeInfoFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        L.t( "BackStack size: " + backStackUtils.getSize() );
    }


    public SearchResultsFragment loadSearchResultsFragment(FragmentInfo tempFragmentInfo) {
        final SearchResultsFragment searchResultsFragment = new SearchResultsFragment();
        this.searchResultsFragment = searchResultsFragment;
        if (tempFragmentInfo == null ) {
            backStackUtils.getBackStack().addFirst(searchResultsFragment.getFragmentInfo());
        } else {
            searchResultsFragment.setTempFragmentInfo(tempFragmentInfo);
            if (tempFragmentInfo.getResponses().isEmpty()) {
                onBackPressed();    //This fragment hasn't loaded before app was closed last time
                return null;
            }
        }
        fm.beginTransaction()
                .replace(mainContainerFragmentId, searchResultsFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        if (getSupportActionBar() != null) { getSupportActionBar().show(); }
        L.t( "BackStack size: " + backStackUtils.getSize() );
        return searchResultsFragment;
    }


    public void onSavePressed(int animeId) {
        if ( appSettings.readUserNickname().isEmpty() || appSettings.readAccessToken().isEmpty() ) {
            loadAuthFragment(null);
            return;
        }
        animeInfoFragment.btnSave.setVisibility(View.GONE);
        animeInfoFragment.loadingCircleBtn.setVisibility(View.VISIBLE);

        L.t("User id read: " + appSettings.readUserId());
        if ( appSettings.readUserId().isEmpty() ) {
            requestManager.requestWhoAmI(this);
            return;
        }
        if ( animeRates == null ) {
            requestManager.requestAllAnimeRates(ActivitySearch.this, appSettings.readUserId(), true);
            return;
        }
        int userRateId = -1;
        for (AnimeRate animeRate : animeRates) {
            if (animeRate.getAnime().getId() == animeId) {
                userRateId = animeRate.getId();
            }
        }
        if ( userRateId == -1 ) {
            L.t("Create UserRate");
            UserRateSend userRateSend = new UserRateSend();
            userRateSend.setUserId(Integer.parseInt(appSettings.readUserId()));
            userRateSend.setTargetId(animeId);
            userRateSend.setTargetType("Anime");
            userRateSend.setScore(0);
            userRateSend.setStatus("planned");
            userRateSend.setEpisodes(0);
            userRateSend.setVolumes(0);
            userRateSend.setChapters(0);
            userRateSend.setText("");
            userRateSend.setRewatches(0);
            requestManager.requestCreateUserRate(ActivitySearch.this, userRateSend.toGsonString());
        } else {
            L.t("Update save button");
            animeInfoFragment.updateSaveButton();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (appSettings.readRestoreLastSession()) {
                backStackUtils.read(this);
                loadEmptyFragment(null);
                onBackPressed();
            }
        } catch (Exception e) { L.t("failed to restore last session"); }
    }


    @Override
    public void onBackPressed() {
        L.t( "BackStack size onBackPressed(): " + backStackUtils.getSize() );
        if (    tabHolderFragment != null &&
                tabHolderFragment.isVisible() &&
                tabHolderFragment.genrePickerPage.getVisibility() == View.VISIBLE)
        {
            tabHolderFragment.btnCancelClicked();
        } else {
            if ( getSupportActionBar() != null ) getSupportActionBar().show();
            if ( backStackUtils.onBackPressedEmpty(this) ) super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final String[] from = new String[] { SEARCH_RESULTS_COLUMN };
        final int[] to = new int[] {android.R.id.text1};
        searchAdapter = new SimpleCursorAdapter(
                this,
                R.layout.item_autocomplete,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.action_search));
        searchView.setSuggestionsAdapter(searchAdapter);
        searchView.setIconifiedByDefault(false);
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView
                .findViewById(android.support.v7.appcompat.R.id.search_src_text);

        searchAutoComplete.setTypeface(MyApp.getAppDefaultFont(), Typeface.NORMAL);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {

            @Override
            public boolean onSuggestionClick(int position) {
                if ( isInternetConnected()) {
                    Cursor cursor = (Cursor) searchView.getSuggestionsAdapter().getItem(position);
                    String pickedName = cursor.getString(cursor.getColumnIndex( SEARCH_RESULTS_COLUMN ));

                    searchView.setQuery(pickedName, false);
                    searchView.clearFocus();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

                    String animeId = SearchResultsFilter.getIdfromAnimeName(allSearchResults, pickedName);
                    getRm().requestAnimeInfo(ActivitySearch.this, animeId, false);
                    getRm().requestRoles(ActivitySearch.this, animeId);
                    getRm().requestRelated(ActivitySearch.this, animeId);
                    getRm().requestScreenshots(ActivitySearch.this, animeId);
                    lastAnimeId = animeId;
                }
                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) { return true; }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                if ( isInternetConnected()) {
                    searchResultsScrollPosition = 0;
                    loadSearchResultsFragment(null);
                    requestManager.requestSearchSubmit( ActivitySearch.this, query );
                    lastSearchQuery = query;
                    searchView.clearFocus();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (isInternetConnectedCheckSilently()) {
                    if (query.length() > 0) {
                        lastSearchQuery = query;
                        if (query.length() > 1 && query.length() % 2 == 0 ) {
                            requestManager.requestSearch(ActivitySearch.this, query);
                        }
                    }
                }
                return false;
            }
        });

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_offer_me) loadTabHolderFragment(null);
        if (item.getItemId() == R.id.action_settings) loadPrefsFragment(null);
        return super.onOptionsItemSelected(item);
    }


    private void populateSearchSuggestionsAdapter() {
        final MatrixCursor c = new MatrixCursor( new String[] { BaseColumns._ID, SEARCH_RESULTS_COLUMN } );
        for (int i = 0; i < suggestions.size(); i++) {
            c.addRow( new Object[] { i, suggestions.get(i) } );
        }
        searchAdapter.changeCursor(c);
    }

    @Override
    public void onFragmentCompleteLoading(FragmentInfo tempFragmentInfo) {

        if (searchView != null) {
            searchView.clearFocus();
        }
        if (tempFragmentInfo == null) return;
        switch (tempFragmentInfo.getFragmentType()) {

            case FragmentInfo.ANIME_INFO :
                animeInfoFragment.updateData(tempFragmentInfo);
                break;

            case FragmentInfo.SEARCH_RESULTS:
                if (searchResultsFragment != null) { searchResultsFragment.updateGrid(tempFragmentInfo); }
                break;

            default : L.t("default on populate response to UI");
        }
    }



    private void handleShortcutIntents() {
        switch (getIntent().getAction())  {

            case "org.shikimori.c7j.rec.OPEN_MENU" :
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadTabHolderFragment(null);
                    }
                }, 100);

                break;


            case "org.shikimori.c7j.rec.OPEN_ANIMELIST" :
                if (!appSettings.readUserNickname().isEmpty()) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                            ShikimoriService.WEBSERVICE_BASE_URL +
                                    "/" +
                                    appSettings.readUserNickname() +
                                    "/list/anime"
                    )));
                    finish();
                } else {
                    UIUtils.showCustomToast(this, R.layout.toast_no_saved_titles_yet, Toast.LENGTH_SHORT);
                }
                break;


            case "org.shikimori.c7j.rec.OPEN_SHIKIMORI" :
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                        ShikimoriService.WEBSERVICE_BASE_URL)));
                finish();
                break;
        }
    }

}
