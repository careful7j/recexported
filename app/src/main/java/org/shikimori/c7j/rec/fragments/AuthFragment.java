package org.shikimori.c7j.rec.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.shikimori.c7j.rec.ActivitySearch;
import org.shikimori.c7j.rec.utils.FragmentInfo;
import org.shikimori.c7j.rec.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AuthFragment extends ABaseFragment {

    public static final String SIGN_UP_URL = "https://shikimori.org/users/sign_up";

    @BindView(R.id.input_login) EditText inputLogin;
    @BindView(R.id.input_password) EditText inputPassword;
    @BindView(R.id.loading_circle_auth) public ProgressBar authProgressBar;
    @BindView(R.id.btn_login) public Button btnLogin;


    @OnClick(R.id.btn_login) void btnLoginClicked() {
        String login = inputLogin.getText().toString();
        String password = inputPassword.getText().toString();
        if (login.isEmpty() || password.isEmpty() || !activitySearch.isInternetConnected()) return;
        authProgressBar.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        activitySearch.getRm().requestAccessToken((ActivitySearch) getActivity(), login, password);
    }


    @OnClick(R.id.btn_register) void btnRegisterClicked() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SIGN_UP_URL + "?utm_source=" + getString(R.string.shikimori_app_id))));
        } catch (ActivityNotFoundException exception) {
            try {
                inputLogin.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SIGN_UP_URL + "?utm_source=" + getString(R.string.shikimori_app_id))));
            } catch (Exception e) {
                if (getView() != null) {
                    Toast.makeText(getView().getContext(),
                            "Can't redirect to: " + SIGN_UP_URL, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private FragmentInfo fragmentInfo = new FragmentInfo(FragmentInfo.AUTH);

    public FragmentInfo getFragmentInfo() { return fragmentInfo; }

    private EditText.OnEditorActionListener onKeyboardEnterListener = new EditText.OnEditorActionListener() {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) btnLoginClicked();
            return true;
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View holder = inflater.inflate(R.layout.fragment_auth, container, false);
        unbinder = ButterKnife.bind(this, holder);
        return holder;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inputLogin.setOnEditorActionListener(onKeyboardEnterListener);
        inputPassword.setOnEditorActionListener(onKeyboardEnterListener);
    }

}
