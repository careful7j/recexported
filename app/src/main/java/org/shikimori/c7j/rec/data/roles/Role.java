
package org.shikimori.c7j.rec.data.roles;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class Role implements Serializable {

    private List<String> roles = new ArrayList<String>();
    @SerializedName("roles_russian")
    private List<String> rolesRussian = new ArrayList<String>();
    private Character character;
    private Person person;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    
    public List<String> getRoles() {
        return roles;
    }

    
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    
    public List<String> getRolesRussian() {
        return rolesRussian;
    }

    
    public void setRolesRussian(List<String> rolesRussian) {
        this.rolesRussian = rolesRussian;
    }

    
    public Character getCharacter() {
        return character;
    }

    
    public void setCharacter(Character character) {
        this.character = character;
    }

    
    public Person getPerson() {
        return person;
    }

    
    public void setPerson(Person person) {
        this.person = person;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Role{" +
                "roles=" + roles +
                ", rolesRussian=" + rolesRussian +
                ", character=" + character +
                ", person=" + person +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
