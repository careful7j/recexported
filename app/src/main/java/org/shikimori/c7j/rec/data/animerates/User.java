
package org.shikimori.c7j.rec.data.animerates;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings({"unused", "Convert2Diamond", "WeakerAccess"})
public class User implements Serializable {

    private Integer id;
    private String nickname;
    private String avatar;
    @SerializedName("image")
    private UserImage userImage;
    private String lastOnlineAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public UserImage getImage() {
        return userImage;
    }

    public void setImage(UserImage userImage) {
        this.userImage = userImage;
    }

    public String getLastOnlineAt() {
        return lastOnlineAt;
    }

    public void setLastOnlineAt(String lastOnlineAt) {
        this.lastOnlineAt = lastOnlineAt;
    }

}
