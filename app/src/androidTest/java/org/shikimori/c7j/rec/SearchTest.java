package org.shikimori.c7j.rec;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class SearchTest {

        @Rule
        public ActivityTestRule<ActivitySearch> mActivityRule = new ActivityTestRule(ActivitySearch.class);

        @Test
        public void mikuoIsDisplayed() {
            onView(withText("Mikuo ")).check(matches(isDisplayed()));
        }

}
